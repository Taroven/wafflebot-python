import sys
import os
import pathlib
from pathlib import Path
import logging
import yaml
import re
import twitchio
from twitchio.ext import commands
from Util.ChannelCache import ChannelCache
import Util.Logger
from Util.FileWatch import FileWatcher
import asyncio
import time

Util.Logger.setup_logging(level=logging.INFO, root=True)
log = logging.getLogger('WaffleBot-Twitch')

class TwitchBot(commands.Bot):
	def __init__(self, **kwargs):
		super().__init__(token = kwargs['token'], prefix=kwargs['prefix'])
		self.wwwAddress = kwargs.pop('www', None)
		self.infoAddress = kwargs.pop('info', None)
		self.operator = kwargs.pop('operator', None)
		if self.operator != None:
			self.operator = self.operator.strip().lower()
		self.wwwAttachmentPath = kwargs.pop('short', 'a')
		self.BaseConfigPath = Path.home() / '.config' / 'WaffleBot'
		self.GlobalDataPath = self.BaseConfigPath / 'GlobalData'
		self.AttachmentPath = self.BaseConfigPath / 'Attachments'
		self.Cache = ChannelCache()
		self.ChannelMap = {}
		self.DisabledCommands = self.GetDisabledCommands()
		self.Commands = {}
		self.Ready = False
		self.CommonBots = [
			'streamelements',
			'nightbot',
			'moobot'
		]
		self.Extensions = [
			'operator',
			'permissions',
			'definitions',
			'quotes',
			'timers',
			'reactions',
			'random',
			'textmanipulation',
			'variables',
			'info',
			'submissions',
			'gambling'
		]

	def LoadExtension(self, ext: str):
		ext = ext.strip().lower()
		if ext not in self.Extensions:
			log.warn(f'Loading Twitch module {ext}. Crash probably impending.')
			self.load_module(f'TwitchModules.{ext}')
			return True

	def ReloadExtension(self, ext: str):
		ext = ext.strip().lower()
		if ext in self.Extensions:
			log.warn(f'Reloading Twitch module {ext}. Crash probably impending.')
			self.reload_module(f'TwitchModules.{ext}')

	async def event_ready(self):
		for ext in self.Extensions:
			self.load_module(f'TwitchModules.{ext}')
		log.info('Twitch logged in and ready.')

	def ReadYaml(self, path: Path):
		if path.exists():
			with open(path, 'r') as stream:
				return yaml.safe_load(stream)
		else:
			return

	def WriteYaml(self, path: Path, data: dict):
		log.debug(f'WriteYaml {path}')
		if not path.parent.exists():
			path.parent.mkdir(parents=True, exist_ok=True)
		with open(path, 'w+') as stream:
			yaml.dump(data, stream)

	def GetYamlName(self, file: str) -> str:
		if not file.endswith('.yaml'):
			return file + '.yaml'
		return file

	def ValidFileName(self, name: str):
		# Define a regular expression pattern to match forbidden characters
		ILLEGAL_NTFS_CHARS = r'[<>:/\\|?*\"]|[\0-\31]'
		# Define a list of forbidden names
		FORBIDDEN_NAMES = [
			'CON', 'PRN', 'AUX', 'NUL',
			'COM1', 'COM2', 'COM3', 'COM4', 'COM5',
			'COM6', 'COM7', 'COM8', 'COM9',
			'LPT1', 'LPT2', 'LPT3', 'LPT4', 'LPT5',
			'LPT6', 'LPT7', 'LPT8', 'LPT9'
		]
		# Check for forbidden characters
		match = re.search(ILLEGAL_NTFS_CHARS, name)
		if match:
			raise ValueError(
			f"Invalid character '{match[0]}' for filename {name}")
		# Check for forbidden names
		if name.upper() in FORBIDDEN_NAMES:
			raise ValueError(f"{name} is a reserved folder name in windows")
		# Check for empty name (disallowed in Windows)
		if name.strip() == "":
			raise ValueError("Empty file name not allowed in Windows")
			# Check for names starting or ending with dot or space
			match = re.match(r'^[. ]|.*[. ]$', name)
		if match:
			raise ValueError(
				f"Invalid start or end character ('{match[0]}')"
				f" in file name {name}"
			)

	def GetGlobalDataPath(self, file: str):	
		path = self.GlobalDataPath / self.GetYamlName(file)
		return path

	def ReadGlobalData(self, file: str):	
		path = self.GlobalDataPath / self.GetYamlName(file)
		log.debug(f'Reading global data from {path}')
		return self.ReadYaml(path)

	def WriteGlobalData(self, file: str | int, data: dict):
		file = str(file)
		try:
			self.ValidFileName(self.GetYamlName(file))
		except ValueError as e:
			log.critical(str(e))
			return
		path = self.GlobalDataPath / self.GetYamlName(file)
		log.debug(f'Writing global data to {path}')
		self.WriteYaml(path, data)
		return True

	def DeleteGlobalData(self, file: str):
		filename = self.GetYamlName(file)
		try:
			self.ValidFileName(self.GetYamlName(filename))
		except ValueError as e:
			log.error(f'invalid filename {file}')
			return
		path = self.GlobalDataPath / filename
		if path.exists():
			log.debug(f'Deleting global data at {str(path)}')
			path.unlink(missing_ok=True)
			return True

	def GetDisabledCommands(self):
		data = self.ReadGlobalData('DisabledCommands')
		if data is None:
			data = {}
		return data

	def SaveDisabledCommands(self):
		self.WriteGlobalData('DisabledCommands', self.DisabledCommands)

	def AddCommand(self, module, command: str):
		command = command.lower()
		if command in self.Commands:
			log.warn(f'Command {command} exists already from module {self.Commands[command]}')
		self.Commands[command] = module

	def GetChannel(self, chan: str):
		chan = chan.lower()
		for channel in self.connected_channels:
			if channel.name.lower() == chan:
				return channel

	async def UpdateChannels(*args):
		bot = None
		event = None
		for arg in args:
			if isinstance(arg, TwitchBot):
				bot = arg
		log.info('Updating channel list')
		data = bot.ReadGlobalData('twitch')
		if data == None:
			data = {}
		channels = []
		channel_map = {}
		for key in data:
			if isinstance(key, str) and key not in channels:
				channels.append(key)
				channel_map[key] = data[key]['guild_id']
		if isinstance(bot.connected_channels, list):
			part_channels = []
			for key in bot.connected_channels:
				if key.name.lower() not in channels:
					part_channels.append(key)
			if len(part_channels) > 0:
				log.info(f'Leaving channels {part_channels}')
				asyncio.create_task(bot.part_channels(part_channels))
		bot.ChannelMap = channel_map
		log.info(f'Joining channels: {channels}')
		await bot.join_channels(channels)

	async def event_command_error(self, context: commands.Context, error: Exception):
		if isinstance(error, commands.CommandNotFound):
			return
		else:
			log.warning(error)

	async def event_ready(self):
		if self.Ready != True:
			log.info(f'Logged into Twitch as {self.nick} at {time.strftime("%X %x %Z")}')
			data = self.ReadGlobalData('twitch')
			if data == None:
				data = {}
				self.WriteGlobalData('twitch', data)
			for ext in self.Extensions:
				self.load_module(f'TwitchModules.{ext}')
			watcher = FileWatcher(self, self.GetGlobalDataPath('twitch'), 5, True, self.UpdateChannels)
			self.Ready = True
		else:
			log.info(f'event_ready occurred at {time.strftime("%X %x %Z")}')

def StartTwitch():
	TwitchConfig = {}
	twitch_config_path = Path.home() / '.config' / 'WaffleBot'
	twitch_config_file = twitch_config_path / 'twitch.yaml'
	twitch_default_id = 'YOUR_TWITCH_BOT_CLIENT_ID_HERE'
	twitch_default_secret = 'YOUR_TWITCH_BOT_SECRET_HERE'
	twitch_default_token = 'YOUR_TWITCH_BOT_TOKEN_HERE'
	twitch_default_config = {
		'prefix': '!',
		'client_id': twitch_default_id,
		'secret': twitch_default_secret,
		'token': twitch_default_token,
		'operator': 123456789,
		'www': 'http://your.domain.here',
		'short': 'a',
		'info': 'info.html?g='
	}
	
	# Create the config file if needed
	if not twitch_config_file.exists():
		twitch_config_path.mkdir(parents=True, exist_ok=True)
		with open(twitch_config_file, 'w+') as stream:
			yaml.dump(twitch_default_config, stream)

	with open(twitch_config_file, 'r') as stream:
		TwitchConfig = yaml.safe_load(stream)
	if TwitchConfig['token'] == twitch_default_token:
		print('Your Twitch bot token is not set. Please edit ' + str(twitch_config_file) + ' to connect. (You may need to add quotes around the token field)')
		print('The "operator" field should be your user ID. Without it set properly you cannot use #op commands.')
		print('The "www" field is used for a local webserver to send attachment permalinks in chat. If you do not run a webserver, delete the line.')
		print('The "short" field is used to change the directory within the web address used for attachments for purposes of shorter links.')
		return

	#FIXME: this is discord.py nomenclature
	bot = TwitchBot(
		token = TwitchConfig.pop('token', None),
		prefix = TwitchConfig.pop('prefix', None),
		www = TwitchConfig.pop('www', None),
		short = TwitchConfig.pop('short', None),
		info = TwitchConfig.pop('info', None),
		operator = TwitchConfig.pop('operator', None))
	bot.run()

StartTwitch()