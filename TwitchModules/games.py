import twitchio
from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule

class Games(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.Commands = {}
		self.API = APIRequest()
		self.AddCommands('game')

	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return
		broadcaster = message.channel.get_chatter(message.channel.name)
		if isinstance(broadcaster, twitchio.chatter.Chatter):
			channel_info = await self.bot.fetch_channels([broadcaster.id])
			if len(channel_info) > 0:
				game_list = await self.bot.fetch_games(ids = [channel_info[0].game_id])
				if len(game_list) > 0:
					igdb_id = game_list[0].igdb_id
					


async def setup(bot):
	await bot.add_cog(Games(bot))