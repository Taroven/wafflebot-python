from pathlib import Path
import re
import logging
import numexpr
from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
from Util.RandomHandler import RandomHandler

Rest = re.compile(r"^\W\w+\s+(.+)")

class Random(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.AddCommands('pick', 'dice', 'math')

	def ListChoices(self, ctx):
		path = self.GetDataPath(ctx)
		_list = list(path.glob('*.yaml'))
		result = "Valid picks:"
		for i in _list:
			result += f' {i.stem}'
		if len(_list) == 0:
			result = 'No choice lists have been created yet.'
		return result

	@commands.command()
	async def pick(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = re.search(r"^\W\w+\s+(\S+)\s+(\S+)", ctx.message.content)
		default = None
		if match is None:
			match = re.search(r"^\W\w+\s+(\S+)", ctx.message.content)
			if match is None:
				await ctx.reply('Usage: !pick [list] (optional iterations) (optional nodupes)')
				return
			else:
				default = True
		word = match.group(1).lower().strip()
		iterations = 1
		pop = False
		if default is None:
			rest = match.group(2)
			args = re.split(r'\s+', rest)
			if len(args) > 0:
				if args[0].isdigit():
					iterations = int(args[0])
				else:
					try:
						iterations = int(RandomHandler(args[0]).Dice())
					except:
						iterations = 1
			if len(args) > 1:
				pop = True

		data = self.ReadData(ctx, word)
		if data is None:
			await ctx.reply(self.ListChoices(ctx))
		else:
			picks = RandomHandler(data).Get(iterations=iterations, pop=pop)
			result = f'Picks from {word}:'
			n = len(picks)
			for i in range(n):
				pick = picks[i]
				result += f' {pick}'
				if n != i+1:
					result += ';'
			await ctx.reply(result)

	@commands.command()
	async def eightball(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		r = RandomHandler()
		result = r.EightBall()
		await ctx.reply(result)

	@commands.command()
	async def dice(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = Rest.search(ctx.message.content)
		if match is None:
			ctx.reply('Usage: !dice [roll] (in D&D notation, ie 2d6, or 1d4+1)')
			return
		try:
			r = RandomHandler(match.group(1).strip())
			dstring = r.DiceString()
			roll = r.Dice()
			await ctx.reply(f'Roll of {dstring}: {roll}')
		except:
			await ctx.reply(f'"{match.group(1).strip()}" could not be parsed.')

	@commands.command()
	async def math(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = re.search(r"^\W\w+\s+(.+)", ctx.message.content)
		if match is None:
			await ctx.reply('Usage: !math [do some math]')
			return
		try:
			result = numexpr.evaluate(match.group(1))
			await ctx.reply(str(result))
		except:
			await ctx.reply('Invalid syntax. Read https://numexpr.readthedocs.io/en/latest/user_guide.html#supported-operators for documentation.')

	# "8ball" doesn't work as a command so we need a custom listener
	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return
		if message.content.lower().startswith('!8ball'):
			r = RandomHandler()
			result = r.EightBall()
			await message.channel.send(f"@{message.author.display_name} " + result)

def prepare(bot: commands.Bot):
	bot.add_cog(Random(bot))