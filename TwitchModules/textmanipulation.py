from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
import re

def Replace(match, content: str):
	return re.sub(re.escape(match.group(1)), match.group(2), content)

Replacements = {
	's/': [re.compile(r's/(.+)/(.+)', re.IGNORECASE), Replace]
}

class TextManipulation(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.Messages = {}

	def TrackMessage(self, message):
		if message.channel.name not in self.Messages:
			self.Messages[message.channel.name] = {}
		self.Messages[message.channel.name][message.author.name] = message

	def GetLastMessage(self, message):
		try:
			return self.Messages[message.channel.name][message.author.name]
		except:
			pass

	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return
		matched = None
		for m, l in Replacements.items():
			if message.content.startswith(m):
				matched = True
				previous = self.GetLastMessage(message)
				match = l[0].match(message.content)
				if match is not None:
					result = l[1](match, previous.content)
					if result is not None:
						await previous.channel.send(previous.author.name + ' meant: ' + result)
						return
		if matched is None:
			self.TrackMessage(message)

def prepare(bot: commands.Bot):
	bot.add_cog(TextManipulation(bot))