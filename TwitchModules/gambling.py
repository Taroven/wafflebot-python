import twitchio
from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
import re
import time
import asyncio
from datetime import timedelta
from Util.Gamble import Gamble
from Util.UserCache import UserCache
from Util.DuelHandler import DuelHandler

points_per_interval = 2
time_interval = 21600

Bidding = re.compile(r"^\W\w+\s+(\d+)")
Dueling = re.compile(r"^\W\w+\s+@?(\w+)\s+(\d+)")
Adding = re.compile(r"^\W\w+\s+@?(\w+)\s+(\-?\d+)")

class GambleResults():
	def __init__(self, data):
		self.data = data

	def Get(self):
		results = getattr(self, self.data['game'])
		return results()

	def roulette(self):
		if self.data['win']:
			payout = self.data['mult'] * self.data['bid']
			result = f'You won {payout} points'
			if self.data['mult'] > 1:
				result += f" with a {self.data['mult']}x multiplier"
			result += '!'
			return result
		else:
			return 'Sorry, you lost. Better luck next time.'

	def slots(self):
		result = "You spun: "
		for i in self.data['slots']:
			result += i + ' '
		if self.data['win']:
			payout = self.data['mult'] * self.data['bid']
			result += f" ; You won {payout} points with a {self.data['mult']}x multiplier!"
			return result
		else:
			result += "\n\nBetter luck next time."
			return result

class Gambling(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.games = Gamble().GetGames()
		self.games_titles = [word.capitalize() for word in self.games]
		self.Cache = UserCache()
		self.Duels = UserCache()
		self.AddCommands('roulette', 'spin', 'points')

	def AddPoints(self, channel, user):
		data = self.ReadData(channel, user)
		if data is None:
			data = {'points': points_per_interval, 'last': time.time()}
			self.WriteData(channel, user, data)
			return data
		ct = time.time()
		time_since_last = ct - data['last']
		if time_since_last > time_interval: # 22 hours since last update
			data['points'] += points_per_interval
			data['last'] = ct
			self.WriteData(channel, user, data)
		return data

	async def DoDueling(self, channel: twitchio.Channel, user_a: str, user_b: str, bid: int):
		data_a = self.AddPoints(channel, user_a)
		data_b = self.AddPoints(channel, user_b)
		if data_a['points'] >= bid and data_b['points'] >= bid:
			duel_result = DuelHandler(user_a, user_b, True).Duel()
			winner = user_a if duel_result['winner'] == user_a else user_b
			loser = user_b if duel_result['winner'] == user_a else user_a
			data_winner = self.AddPoints(channel, winner)
			data_loser = self.AddPoints(channel, loser)
			data_winner['points'] += bid
			data_loser['points'] -= bid
			self.WriteData(channel, winner, data_winner)
			self.WriteData(channel, loser, data_loser)
			await channel.send(duel_result['battle'] + f' Points won: {bid}')
		else:
			await channel.send(f'The duel between {user_a} and {user_b} has been called off. One of them no longer has enough points.')

	async def DoGambling(self, ctx: commands.Context, game, bid):
		data = self.AddPoints(ctx.channel.name, ctx.author.name)
		if bid > data['points']:
			time_until_next = int(data['last']) + time_interval - int(time.time())
			time_delta = timedelta(seconds=time_until_next)
			await ctx.reply(f"You have {data['points']} points remaining. You will receive {points_per_interval} points in {time_delta}.")
			return
		gamble = Gamble(game=game, bid=bid)
		result = gamble.Play()
		result_output = GambleResults(result).Get()
		if result['win']:
			data['points'] += result['mult'] * bid
		else:
			data['points'] -= bid
		self.WriteData(ctx.channel.name, ctx.author.name, data)
		await ctx.reply(result_output)

	def IsDuelPending(self, channel, user):
		ct = time.time()
		ttl = self.Duels.GetProp(channel, user, 'ttl')
		if ttl is None or ct > ttl:
			return False
		else:
			return True

	@commands.command()
	async def accept(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		if self.IsDuelPending(ctx.channel.name, ctx.author.name):
			partner = self.Duels.GetProp(ctx.channel.name, ctx.author.name, 'partner')
			bid = self.Duels.GetProp(ctx.channel.name, ctx.author.name, 'bid')
			self.Duels.Remove(ctx.channel.name, ctx.author.name)
			await self.DoDueling(ctx.channel, partner, ctx.author.name, bid) 
		else:
			await ctx.reply('You have no pending duels.')

	@commands.command()
	async def reject(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		if self.IsDuelPending(ctx.channel.name, ctx.author.name):
			partner = self.Duels.GetProp(ctx.channel.name, ctx.author.name, 'partner')
			self.Duels.Remove(ctx.channel.name, ctx.author.name)
			await ctx.reply(f'Your duel with {partner} has been called off.')
		else:
			await ctx.reply('You have no pending duels.')

	@commands.command()
	async def duel(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = Dueling.search(ctx.message.content)
		if match is None:
			await ctx.reply('Usage: !duel [user] [wager] - Both users must have enough points.')
		partner = match.group(1)
		bid = int(match.group(2))
		if partner.lower() == ctx.author.name.lower():
			await ctx.reply('You cannot duel yourself.')
			return
		partner_exists = self.ReadData(ctx.channel.name, partner)
		if partner_exists is None:
			await ctx.reply(f'{partner} either has never chatted or does not exist. Please check your spelling and try again.')
		if self.IsDuelPending(ctx.channel.name, partner):
			await ctx.reply(f'{partner} already has a pending duel.')
			return
		user_data = self.AddPoints(ctx.channel.name, ctx.author.name)
		partner_data = self.AddPoints(ctx.channel.name, partner)
		if user_data['points'] < bid or partner_data['points'] < bid:
			if user_data['points'] < bid and partner_data['points'] < bid:
				await ctx.reply(f"Neither of you has enough points. You have {user_data['points']} and {partner} has {partner_data['points']}.")
				return
			elif user_data['points'] < bid:
				await ctx.reply(f"You do not have enough points. You have {user_data['points']} and {partner} has {partner_data['points']}.")
				return
			else:
				await ctx.reply(f"{partner} does not have enough points. You have {user_data['points']} and {partner} has {partner_data['points']}.")
				return
		# now that that's out of the way...
		ttl = time.time() + 60
		self.Duels.Set(ctx.channel.name, partner, {'partner': ctx.author.name, 'bid': bid, 'ttl': ttl}) # we set this backwards so sending a challenge doesn't interfere with incoming challenges
		await ctx.reply(f'@{partner}, {ctx.author.name} has challenged you to a duel for {bid} points! You have 60 seconds to !accept or !reject.')

	@commands.command()
	async def slots(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = Bidding.search(ctx.message.content)
		if match is None:
			await self.DoGambling(ctx, 'slots', 1)
		else:
			bid = int(match.group(1))
			await self.DoGambling(ctx, 'slots', bid)

	@commands.command()
	async def roulette(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = Bidding.search(ctx.message.content)
		if match is None:
			await self.DoGambling(ctx, 'roulette', 1)
		else:
			bid = int(match.group(1))
			await self.DoGambling(ctx, 'roulette', bid)

	@commands.command()
	async def spin(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = Bidding.search(ctx.message.content)
		if match is None:
			await self.DoGambling(ctx, 'roulette', 1)
		else:
			bid = int(match.group(1))
			await self.DoGambling(ctx, 'roulette', bid)

	@commands.command()
	async def addpoints(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel.name, ctx.author.name, 'Admin')
		if not permitted:
			return
		match = Adding.search(ctx.message.content)
		if match is None:
			await ctx.reply('Usage: !addpoints [user] [amount] - Amount may be negative to subtract points.')
			return
		user = match.group(1)
		points = int(match.group(2))
		user_exists = self.ReadData(ctx.channel.name, user)
		if user_exists is None:
			await ctx.reply('{user} either has never chatted or does not exist. Please check your spelling and try again.')
			return
		user_data = self.AddPoints(ctx.channel.name, user)
		user_data += points
		self.WriteData(ctx.channel.name, user, user_data)
		await ctx.reply('{user} has been given {points} points.')

	@commands.command()
	async def givepoints(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = Adding.search(ctx.message.content)
		if match is None:
			await ctx.reply('Usage: !givepoints [user] [amount] - You must have enough points to transfer.')
			return
		user = match.group(1)
		points = abs(int(match.group(2)))
		user_exists = self.ReadData(ctx.channel.name, user)
		if user_exists is None:
			await ctx.reply('{user} either has never chatted or does not exist. Please check your spelling and try again.')
			return
		user_data = self.AddPoints(ctx.channel.name, user)
		sender_data = self.AddPoints(ctx.channel.name, ctx.author.name)
		user_data['points'] += points
		sender_data['points'] -= points
		self.WriteData(ctx.channel.name, user, user_data)
		self.WriteData(ctx.channel.name, ctx.author.name, sender_data)
		await ctx.reply('You have given {points} points to @{user}.')

	@commands.command()
	async def points(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		data = self.AddPoints(ctx.channel.name, ctx.author.name)
		time_until_next = int(data['last']) + time_interval - int(time.time())
		time_delta = timedelta(seconds=time_until_next)
		await ctx.reply(f"You have {data['points']} points remaining. You will receive {points_per_interval} points in {time_delta}.")

	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return
		user = message.author.name
		channel = message.channel.name
		if self.Cache.GetProp(channel, user, 'last') is None:
			data = self.AddPoints(channel, user)
			self.Cache.SetProp(channel, user, 'last', data['last'])
		ct = time.time()
		self.Cache.SetProp(channel, user, 'seen', ct)
		if ct - self.Cache.GetProp(channel, user, 'last') > time_interval:
			data = self.AddPoints(channel, user)
			self.Cache.SetProp(channel, user, 'last', data['last'])

def prepare(bot: commands.Bot):
	bot.add_cog(Gambling(bot))