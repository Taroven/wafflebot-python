from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
from Util.APIParse import APIRequest
from Util.Substring import Substring
import re

# save a few cycles
Print = re.compile(r"^[=#]\s?(\w+)")
Increment = re.compile(r"^(\w+)\s?([\+\-]{2})")
Set = re.compile(r"^(\w+)\s?([\+\-=])\s?(\d+)")

class Variables(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.AddCommands('addvariable', 'unvariable')

	@commands.command()
	async def addvariable(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel.name, ctx.message.author.name, 'Op')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return
		match = re.search(r'\W\w+\s+(.+)', ctx.message.content)
		if match is not None:
			chunks = match.group(1).strip().split(' ')
			name = chunks.pop(0).lower()
			data = { 'val': 0 }
			output = f'Variable {name} created'
			if len(chunks) > 0:
				data['alias'] = ' '.join(chunks)
				output += f' with alias {data["alias"]}'
			output += ' - Use `{name}++` to increment by 1, `{name}--` to decrement by 1, `{name}+X` to add `X`, `{name}-X` to subtract `X`, or `{name}=X` to set to `X`.'
			self.WriteData(ctx.channel.name, name, data)
			await ctx.reply(output)
		else:
			await ctx.reply('Usage: {ctx.prefix}addvariable [name] [optional alias] (initial values are not supported on Twitch)')

	@commands.command()
	async def unvariable(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel.name, ctx.message.author.name, 'Op')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return
		match = re.search(r'\W\w+\s+(\w+)', ctx.message.content)
		if match is not None:
			name = match.group(1).strip().lower()
			self.DeleteData(ctx.channel.name, name)
			await ctx.reply(f'Variable {name} has been deleted.')
		else:
			await ctx.reply(f'Usage: {ctx.prefix}unvariable [name]')

	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return
		isPrint = Print.search(message.content)
		if isPrint is not None:
			var = isPrint.group(1).lower()
			data = self.ReadData(message.channel.name, var)
			if data is not None:
				name = var if 'alias' not in data else data['alias']
				await message.channel.send(f'{name}: {data["val"]}')
				return

		permitted = self.IsCommandPermitted(message.channel.name, message.author.name, 'Op')
		if not permitted:
			return

		isIncrement = Increment.search(message.content)
		if isIncrement is not None:
			var = isIncrement.group(1).lower()
			data = self.ReadData(message.channel.name, var)
			if data is not None:
				name = name = var if 'alias' not in data else data['alias']
				if isIncrement.group(2) == '++':
					data["val"] += 1
				elif isIncrement.group(2) == '--':
					data["val"] -= 1
				self.WriteData(message.channel.name, var, data)
				await message.channel.send(f'{name}: {data["val"]}')
				return

		isSet = Set.search(message.content)
		if isSet is not None:
			var = isSet.group(1).lower()
			data = self.ReadData(message.channel.name, var)
			if data is not None:
				name = name = var if 'alias' not in data else data['alias']
				newval = int(isSet.group(3))
				if isSet.group(2) == '+':
					data["val"] += newval
				elif isSet.group(2) == '-':
					data["val"] -= newval
				elif isSet.group(2) == '=':
					data["val"] = newval
				self.WriteData(message.channel.name, var, data)
				await message.channel.send(f'{name}: {data["val"]}')
				return

def prepare(bot: commands.Bot):
	bot.add_cog(Variables(bot))