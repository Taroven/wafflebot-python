import twitchio
from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
import re

class Permissions(ChatModule):
	def __init__(self, bot):
		self.shortcuts = {
				'a': 1,
				'o': 2,
				't': 3,
				'v': 4
			}
		super().__init__(bot)

	def ReadPermissions(self, context: commands.Context | twitchio.Message | twitchio.Channel):
		channel = None
		if isinstance(context, twitchio.Channel):
			channel = context.name
		else:
			channel = context.channel.name
		permissions = self.bot.Cache.AddChannel(channel, channel)
		savedData = self.ReadData(context, 'permissions.twitch')
		if savedData == None:
			savedData = self.WritePermissions(context, permissions)
		for role_id, role_perm in savedData['roles'].items():
			self.bot.Cache.SetRoleLevel(channel, role_id, role_perm)
		for user_id, user_perm in savedData['users'].items():
			self.bot.Cache.SetUserLevel(channel, user_id, user_perm)
		return self.bot.Cache.AddChannel(channel, channel)

	def WritePermissions(self, context: commands.Context | twitchio.Message | twitchio.Channel, data):
		if 'users' not in data:
			return
		if 'roles' not in data:
			return
		channel = None
		if isinstance(context, twitchio.Channel):
			channel = context.name
		else:
			channel = context.channel.name
		self.WriteData(channel, 'permissions.twitch', data)
		return self.bot.Cache.AddChannel(channel, channel)

	@commands.Cog.event()
	async def event_join(self, channel, user):
		if channel.name not in self.Initialized:
			self.Initialized[channel.name] = True
			self.ReadPermissions(channel)

	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo or not self.IsCommandPermitted(message.channel, message.author.name, 'Op'):
			return
		result = re.search(r"^[\+\-](\w+)\s+@?(\w+)", message.content)
		if result is None:
			return
		cache = self.bot.Cache
		target = result.group(1).lower()
		user = result.group(2).lower()
		userLevel = cache.GetUserLevel(message.channel.name, user)
		senderLevel = cache.GetUserLevel(message.channel.name, message.author.name)
		if target in self.shortcuts and message.content.startswith('+'):
			target = self.shortcuts[target]
			if target <= senderLevel:
				await message.channel.send('Cannot set a user to your own role or greater.')
				return
			if userLevel <= senderLevel:
				await message.channel.send(f'{user} has a role equal or greater to your own.')
				return
			permissionData = self.ReadPermissions(message)
			permissionData['users'][user] = target
			self.WritePermissions(message, permissionData)
			await message.channel.send(f'Set {user} -> {cache.UserLevelNames[target]}')
		elif target in self.shortcuts and message.startswith('-'):
			if userLevel == cache.UserLevels['User']:
				await message.channel.send('{user} has no role to remove.')
				return
			if userLevel <= senderLevel:
				await message.channel.send('{user} has a role equal or greater to your own.')
				return
			permissionData = self.ReadPermissions(message)
			permissionData['users'].pop(user, None)
			self.WritePermissions(message, permissionData)
			await message.channel.send(f'Cleared permissions for {user} (was {cache.UserLevelNames[userLevel]})')

def prepare(bot: commands.Bot):
	bot.add_cog(Permissions(bot))