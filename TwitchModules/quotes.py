import discord
from typing import Optional
import re
import random
import mimetypes
import logging
from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule

log = logging.getLogger('WaffleBot-Twitch.quotes')

class Quotes(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.AddCommands('quote', 'unquote', 'addquote')
	
	def Delete(self, channel, quote_id):
		data = self.ReadData(channel, quote_id)
		if data != None:
			if 'attachment' in data:
				self.AttachmentHandler.UnregisterAttachment('Quotes', data['attachment'])
			return self.DeleteData(channel, quote_id)

	def GetQuoteList(self, channel):
		path = self.GetDataPath(channel)
		file_list = list(path.glob('*.yaml'))
		quote_list = []
		for file in file_list:
			try:
				quote_list.append(int(file.stem))
			except:
				pass
		return quote_list

	def GetNextQuoteID(self, channel):
		quote_list = self.GetQuoteList(channel)
		i = 1
		while True:
			if i in quote_list:
				i += 1
			else:
				return i

	def GetRandomQuote(self, channel):
		quote_list = self.GetQuoteList(channel)
		numquotes = len(quote_list)
		if numquotes > 0:
			r = random.choice(quote_list)
			return self.ReadData(channel, str(r))

	def GetSpecificQuote(self, channel, id: int):
		quote_list = self.GetQuoteList(channel)
		path = self.GetDataPath(channel)
		filename = str(id) + '.yaml'
		file = path / filename
		if file.exists():
			return self.ReadData(channel, str(id))

	@commands.command()
	async def unquote(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel, ctx.message.author.name, 'Trusted')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return

		match = re.search(r"^\W\w+\s+(\d+)", ctx.message.content)
		if match != None:
			quote_id = int(match.group(1))
			quote_list = self.GetQuoteList(ctx.channel)
			if quote_id in quote_list:
				if self.Delete(ctx.channel, quote_id):
					await ctx.reply('Quote #' + str(quote_id) + ' has been deleted.')
				else:
					await ctx.reply('Quote #' + str(quote_id) + ' could not be deleted (please contact the bot operator).')
			else:
				await ctx.reply('Quote #' + str(quote_id) + ' could not be found.')
		else:
			await ctx.reply(f'Usage: {ctx.prefix}unquote [id]')
			return

	@commands.command()
	async def addquote(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel, ctx.message.author.name, 'Trusted')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return

		match = re.search(r"^.addquote\s+(.+)", ctx.message.content)
		if match != None:
			quote = match.group(1).strip()
			quote = re.sub(r'\s?;\s?', '\n', quote)
			next_id = self.GetNextQuoteID(ctx.channel)
			quote_dict = {
				'author': ctx.author.name,
				'index': next_id,
				'content': quote
			}
			self.WriteData(ctx.channel, next_id, quote_dict)
			await ctx.reply('Quote #' + str(next_id) + ' has been added.')
		else:
			await ctx.reply(f'Usage: {ctx.prefix}addquote [text]')
			return

	@commands.command()
	async def quote(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		match = re.search(r"^.\w+\s+(\d+)", ctx.message.content)
		quote_id = None
		if match != None:
			quote_id = int(match.group(1))

		quote = None
		if quote_id == None:
			quote = self.GetRandomQuote(ctx.channel)
		else:
			quote = self.GetSpecificQuote(ctx.channel, quote_id)
		if quote != None:
			content = f'Quote #{quote["index"]}: '
			if 'content' in quote:
				content = content + quote['content']
			if 'attachment' in quote:
				attach_path = self.GetAttachmentAddress(quote['attachment'])
				if attach_path != None:
					if 'content' in quote:
						content += '; '
					content += attach_path
			content = re.sub(r'\s?\n\s?', '; ', content) # convert newline to semicolon
			content = re.sub(r'```', '', content) # strip codeblocks
			await ctx.reply(content)
		else:
			if quote_id != None:
				await ctx.reply('Could not find a quote with that ID.')
			else:
				await ctx.reply('Could not find any quotes.')

def prepare(bot: commands.Bot):
	bot.add_cog(Quotes(bot))