from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
import re
import asyncio
import time
import logging

log = logging.getLogger('WaffleBot-Twitch.reaction')

class Reactions(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.Reactions = {}
		self.AddCommands('react', 'unreact')

	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return
		author = message.author.name.lower()
		channel = message.channel.name.lower()
		if author in self.bot.CommonBots:
			return
		if channel in self.Reactions:
			cache = self.Reactions[channel]
			for reaction in cache:
				if reaction in message.content.lower():
					current = time.time()
					if 'last' in cache[reaction]:
						if current - cache[reaction]['last'] < 60:
							return
					cache[reaction]['last'] = current
					await message.channel.send(cache[reaction]['content'])

	@commands.Cog.event()
	async def event_join(self, channel, user):
		if channel.name not in self.Initialized:
			self.Initialized[channel.name] = True
			data = self.ReadData(channel, 'reactions')
			if data is not None:
				self.Reactions[channel.name.lower()] = data

	@commands.command()
	async def react(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx, ctx.message.author.name, 'Admin')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return
		result = re.search(r'^\W\w+\s+\"(.+)\"\s+(.+)', ctx.message.content)
		if result is None:
			await ctx.reply('Usage: !react "message content" [Message to react with] - Quotes are required for the first portion. Reactions are not case sensitive and will occur every 60 seconds at most.')
			return
		data = self.ReadData(ctx, 'reactions')
		if data is None:
			data = {}
		data[result.group(1).lower()] = {'content': result.group(2)}
		self.WriteData(ctx, 'reactions', data)
		self.Reactions[ctx.channel.name.lower()] = data
		await ctx.reply(f'I will now react to "{result.group(1).lower()}". Use !unreact "{result.group(1).lower()}" to clear.')

	@commands.command()
	async def unreact(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx, ctx.author.name, 'Admin')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return
		result = re.search(r'^\W\w+\s+\"?(.+)\"?', ctx.message.content)
		if result is None:
			await ctx.reply('Usage: !unreact "message content" - Quotes are optional.')
			return
		data = self.ReadData(ctx, 'reactions')
		if data is None:
			data = {}
		removed = data.pop(result.group(1).lower(), None)
		self.WriteData(ctx, 'reactions', data)
		self.Reactions[ctx.channel.name.lower()] = data
		if removed is None:
			await ctx.reply(f'Could not find a reaction to remove.')
		else:
			await ctx.reply(f'I will no longer react to "{result.group(1).lower()}".')

def prepare(bot: commands.Bot):
	bot.add_cog(Reactions(bot))