import twitchio
from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
import re

OneWord = re.compile(r"^\W\w+\s+(\w+)")

class Submissions(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.AddCommands('nextsub', 'submit')

	@commands.command()
	async def nextsub(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel.name, ctx.author.name, 'Admin')
		if not permitted:
			return
		data = self.ReadData(ctx, 'current')
		if data is None:
			await ctx.reply('Submissions are not currently open.')
			return
		if data['i'] > len(data['submissions']) - 1:
			await ctx.reply('There are no more submissions in the pool.')
			return
		current = data['submissions'][data['i']]
		output = f'Submission {data["i"] + 1} by {current[0]}:\n{current[1]}'
		await ctx.reply(output)
		data['i'] += 1
		self.WriteData(ctx, 'current', data)

	@commands.command()
	async def submit(self, ctx: commands.Context):
		data = self.ReadData(ctx, 'current')
		if data is None:
			await ctx.reply('Submissions are not currently open.')
			return
		match = OneWord.search(ctx.message.content)
		if match is None:
			await ctx.reply('You should add some content to submit.')
			return
		elif len(match.group(1).strip()) < 8:
			await ctx.reply('Submission too short.')
			return
		data['submissions'].append([ctx.author.name, match.group(1).strip()])
		await ctx.reply('Your submission has been added to the list.')

def prepare(bot: commands.Bot):
	bot.add_cog(Submissions(bot))