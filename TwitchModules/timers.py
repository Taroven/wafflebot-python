import twitchio
from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
from Util.FileWatch import FileWatcher
import re
import asyncio
import time
import logging

log = logging.getLogger('WaffleBot-Twitch.timers')

class Timers(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.Timers = {}
		self.Activity = {}
		self.Running = True
		asyncio.create_task(self.StartLoop())

	def UpdateActivity(self, channel):
		if channel not in self.Activity:
			self.Activity[channel] = {}
		cache = self.Activity[channel]
		current = time.time()
		cleanup = []
		for user in cache:
			if current - cache[user] > 600:
				cleanup.append(user)
		for user in cleanup:
			cache.pop(user, None)
		return len(cache)

	def UpdateTimers(*args, **kwargs):
		this = None
		for obj in args:
			if isinstance(obj, Timers):
				this = obj
		channel = kwargs['channel']
		if isinstance(channel, twitchio.Channel):
			channel = channel.name.lower()
		else:
			channel = channel.lower()
		path = this.GetDataPath(channel)
		timer_list = list(path.glob('*.yaml'))
		this.Timers[channel] = {}
		for timer in timer_list:
			timer_dict = this.ReadData(channel, timer.stem)
			if timer_dict:
				timer_dict['channel'] = channel
				timer_dict['triggered'] = time.time()
				this.Timers[channel][timer_dict['name']] = timer_dict

	async def TimerLoop(self):
		current = time.time()
		for channel_name, channel_timers in self.Timers.items():
			for timer_name, cache in channel_timers.items():
				if channel_name in self.bot.ChannelMap: # make sure we're actually supposed to send messages to a given channel
					interval = cache['interval'] * 60
					if self.UpdateActivity(cache['channel']) >= cache['active'] and current - cache['triggered'] > interval:
						channel = self.bot.GetChannel(cache['channel'])
						if channel:
							cache['triggered'] = current
							await channel.send(cache['content'])
		await asyncio.sleep(5)

	async def StartLoop(self):
		while self.Running:
			await self.TimerLoop()

	@commands.Cog.event()
	async def event_join(self, channel, user):
		if channel.name not in self.Initialized:
			self.Initialized[channel.name] = True
			self.watcher = FileWatcher(self, self.GetDataPath(channel.name) / '.modified', 5, False, self.UpdateTimers, extra = {'channel': channel})

	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return
		author = message.author.name.lower()
		channel = message.channel.name.lower()
		if author in self.bot.CommonBots:
			return
		if channel not in self.Activity:
			self.Activity[channel] = {}
		self.Activity[channel][author] = time.time()
		self.UpdateActivity(channel)

def prepare(bot: commands.Bot):
	bot.add_cog(Timers(bot))