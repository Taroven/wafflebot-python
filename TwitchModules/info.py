from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
from urllib.parse import urljoin
import re

class Info(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.AddCommands('info')

	@commands.command()
	async def info(self, ctx: commands.Context):
		try:
			url = urljoin(self.bot.wwwAddress, '/' + self.bot.infoAddress + str(self.GetGuild(ctx)))
			await ctx.reply(url)
		except:
			await ctx.reply('The bot operator has not enabled this functionality.')

def prepare(bot: commands.Bot):
	bot.add_cog(Info(bot))