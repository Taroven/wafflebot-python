from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
from Util.APIParse import APIRequest
from Util.Substring import Substring
import re

# save a few cycles
Defining = re.compile(r"^\W\w+\s+(\w+)\s+(.+)")
OneWord = re.compile(r"^\W\w+\s+(\w+)")
TwoWord = re.compile(r"^\W\w+\s+(\w+)\s+(\w+)")
Def = re.compile(r"^[\?\!]\s?(\w+)")

class Definitions(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.Commands = {}
		self.API = APIRequest()
		self.AddCommands('define', 'undefine', 'tocommand', 'alias')

	def Delete(self, channel, word):
		data = self.ReadData(channel, word)
		if data != None:
			if 'attachment' in data:
				self.AttachmentHandler.UnregisterAttachment('Definitions', data['attachment'])
			return self.DeleteData(channel, word)

	@commands.command()
	async def define(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel.name, ctx.message.author.name, 'Trusted')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return

		match = Defining.search(ctx.message.content)
		word = None
		content = None
		if match != None:
			word = match.group(1).lower().strip()
			content = match.group(2)
		else:
			await ctx.reply(f'Usage: {ctx.prefix}define [word] [definition] - Use ";" to add line breaks for Discord')
			return
		if not word.isalnum():
			await ctx.reply(f'`{word}` cannot be used as a definition.')
			return
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await ctx.reply(f'`{word}` cannot be used as a definition.')
			return

		finaldef = {
			'author': ctx.author.name,
			'word': word,
			'content': re.sub(r'\s?;\s?', '\n', content)
		}

		self.WriteData(ctx.channel.name, word, finaldef)
		await ctx.reply(f'Definition for `{word}` added.')
		self.AttachmentHandler.RemoveUnusedAttachments()
	
	@commands.command()
	async def alias(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel, ctx.message.author.name, 'Trusted')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return

		match = TwoWord.search(ctx.message.content)
		if match == None:
			await ctx.reply('Usage: !alias [word] [otherword]')
			return
		word = match.group(1).lower()
		newword = match.group(2).lower()
		data = self.ReadData(ctx.channel.name, word)
		if data is None:
			await ctx.reply(f'{word} does not exist as a definition.')
			return
		newdata = {'redirect': word}
		self.WriteData(ctx.channel.name, newword, newdata)
		await ctx.reply(f'Defined ?{newword} as an alias of ?{word}.')

	@commands.command()
	async def undefine(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel, ctx.message.author.name, 'Trusted')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return
		
		match = OneWord.search(ctx.message.content)
		word = None
		if match != None:
			word = match.group(1).lower().strip()
		else:
			await ctx.reply(f'Usage: {ctx.prefix}undefine [word]')
			return

		if not word.isalnum():
			await ctx.reply(f'`{word}` cannot be used as a definition.')
			return
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await ctx.reply(f'`{word}` cannot be used as a definition.')
			return
		deleted = self.Delete(ctx.channel.name, word)
		if deleted == True:
			await ctx.reply(f'Definition for `{word}` deleted.')
		else:
			await ctx.reply(f'No definition for `{word}` found.')
		self.AttachmentHandler.RemoveUnusedAttachments()

	@commands.command()
	async def uncommand(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel, ctx.message.author.name, 'Op')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return
		result = OneWord.search(ctx.message.content)
		if result is None:
			await ctx.reply('Usage: !tocommand [definition] (must be a valid definition)')
			return
		word = result.group(1).lower()
		data = self.ReadData(ctx, '.commands')
		data.pop(word, None)
		self.WriteData(ctx, '.commands')
		await ctx.reply(f'Removed !{word} (?{word} still exists).')

	@commands.command()
	async def tocommand(self, ctx: commands.Context):
		if ctx.message.echo:
			return
		permitted = self.IsCommandPermitted(ctx.channel, ctx.message.author.name, 'Op')
		if not permitted:
			await ctx.reply('You do not have permission to do this.')
			return
		path = self.GetDataPath(ctx.channel)
		definition_files = list(path.glob('*.yaml'))
		definition_list = []
		for fp in definition_files:
			stem = str(fp.stem)
			if not stem.startswith('.'):
				definition_list.append(stem)
		result = OneWord.search(ctx.message.content)
		if result is None:
			await ctx.reply('Usage: !tocommand [definition] (must be a valid definition)')
			return
		word = result.group(1).lower()
		if word not in definition_list:
			await ctx.reply('Usage: !tocommand [definition] (must be a valid definition)')
			return
		if word in self.bot.Commands:
			await ctx.reply(f'Cannot set "{word}" as a command, as it would overwrite built-in functionality.')
			return
		else:
			data = self.ReadData(ctx, '.commands')
			if data is None:
				data = {}
			data[word] = True
			channel = ctx.channel.name.lower()
			if channel not in self.Commands:
				self.Commands[channel] = {}
			self.Commands[channel][word] = True
			self.WriteData(ctx, '.commands', data)
			await ctx.reply(f'Added ?{word} as command !{word}')

	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return

		result = Def.search(message.content)
		if result is None: #silent fail for messages like "???" or "?!"
			return
		else:
			word = result.group(1).lower()
			if not word.isalnum(): #silent fail if we're not passing alphanumeric
				return
			if message.content.startswith('!'):
				if word not in self.Commands[message.channel.name.lower()]:
					return
			definition = self.ReadData(message.channel.name, word)
			if definition is None: #silent fail if we don't have an existing definition
				return
			if 'redirect' in definition:
				definition = self.ReadData(message.channel.name, definition['redirect'])
				if definition is None:
					return
			content = ''
			if 'api' in definition:
				content = await self.API.BuildFromAPI(definition)
				print(content)
			elif 'content' in definition:
				content = definition['content']

			if 'attachment' in definition:
				attach_path = self.GetAttachmentAddress(definition['attachment'])
				if attach_path != None:
					if 'content' in definition:
						content = content + '; ' + attach_path
					else:
						content = attach_path
				elif 'content' in definition: # this should never happen, but if it does we're highly annoyed
					#log.error(f'Could not find attachment for definition `{word}` in channel `{message.channel.name}`')
					pass
				else:
					#log.error(f'Invalid definition `{word}` in channel `{message.channel.name}` - no content and missing attachment')
					return
			content = str(Substring(message, content))
			content = re.sub(r'\s?\n\s?', '; ', content)
			await message.channel.send(content)

	@commands.Cog.event()
	async def event_join(self, channel, user):
		if channel.name not in self.Initialized:
			self.Initialized[channel.name] = True
			commands = self.ReadData(channel, '.commands')
			if commands is None:
				commands = {}
			self.Commands[channel.name.lower()] = commands

def prepare(bot: commands.Bot):
	bot.add_cog(Definitions(bot))