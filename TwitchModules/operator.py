from twitchio.ext import commands
from TwitchUtil.ChatModule import ChatModule
import re

class Operator(ChatModule):
	@commands.Cog.event()
	async def event_message(self, message):
		if message.echo:
			return
		result = re.search(r"^\#op\s+(\w+)", message.content)
		if result is None:
			return
		operator = False
		if message.author.name.lower() == self.bot.operator:
			operator = True
		elif not self.IsCommandPermitted(message.channel, message.author.name, 'Owner'):
			return
		word = result.group(1).lower()
		chunks = re.split(' +', message.content)
		try:
			chunks.pop(0)
			chunks.pop(0)
		except:
			pass
		
		if word == 'disable' and len(chunks) > 0:
			command = chunks[0].lower()
			self.DisableCommand(command, message.channel)
			return message.channel.send(f'"{command}" has been disabled.')

		if word == 'enable' and len(chunks) > 0:
			command = chunks[0].lower()
			self.EnableCommand(command, message.channel)
			return message.channel.send(f'"{command}" has been enabled.')

def prepare(bot: commands.Bot):
	bot.add_cog(Operator(bot))