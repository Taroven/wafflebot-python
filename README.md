# Wafflebot-Python

## What is Waffles?
Wafflebot (Waffles from here on out) is a combined Discord and Twitch bot written in Python. Its goal is simple: Provide a seamless chat experience between both platforms. Data is shared between both sides of the bot with as few compromises as possible.

## What can Waffles do?
Quite a lot. Check out [the documentation](https://gitlab.com/Taroven/wafflebot-python/-/wikis/home) for command lists and usage examples separated by category.

## I want to add Waffles to my server!
If you are comfortable with not having absolute control over the bot internals I run a central, remotely hosted instance. The only special access I have as the operator are a few commands to change the "now playing" line and fix up Twitch channel linking when needed. Contact me on Discord (`@taroven`) for an invite link - You're welcome to share the link on the condition that you notify me of who is adding Waffles. I don't need to be in a server that Waffles is added to, but I would love to see how it's being used.

## I want to run Waffles myself!
Go for it. This project is open for personal use. Waffles runs on Python 3.10+ (perhaps lower, untested). The only dependencies should be `discord.py` and `twitchio` from Pip, but if anything is missing it should be pretty obvious from error messages. A Discord developer account, app, and oauth token is required (all available via the developer dashboard). For Twitch you'll need a bot account and oauth token for it (more annoying to get but not egregious).

## I have suggestions, questions, or concerns about Waffles.
Contact me as per above. I always welcome feedback. If you're rolling your own instance, be aware that I can't provide support for code that I did not write.

## I found a bug!
Contact me on Discord or file an issue on Gitlab.

## I want to support development!
Toss me a sub on [Twitch](https://twitch.tv/taroven) and get some emotes in the process, or [Patreon](https://patreon.com/Taroven) if you would like to support me directly.
