import os
from hashlib import blake2b
import pathlib
from pathlib import Path
import yaml
import discord
import re
import logging
from typing import Any, List, Union
from hashlib import blake2b

from discord.ext import commands
from discord.ui import MentionableSelect
from discord import app_commands, ui, Interaction

from Util.Attachments import AttachmentHandler

log = logging.getLogger('DiscordUtil.ChatModule')

class ChatModule(commands.Cog):
	def __init__(self, bot):
		super().__init__()
		self.bot = bot
		self.dataPath = Path.home() / '.config' / 'WaffleBot' / self.qualified_name
		self.dataPath.mkdir(parents=True, exist_ok=True)
		self.AttachmentHandler = AttachmentHandler(bot)
		log.info(f'{self.qualified_name} initialized')

	def GetDocLink(self):
		return 'https://gitlab.com/Taroven/wafflebot-python/-/wikis/' + self.qualified_name.lower()

	def GetGuild(self, guild: int | discord.Guild | discord.ext.commands.Context) -> int:
		guild_id = 0
		if isinstance(guild, int):
			guild_id = guild
		elif isinstance(guild, discord.Guild):
			guild_id = guild.id
		elif isinstance(guild, discord.ext.commands.Context):
			guild_id = guild.guild.id
		if guild_id != 0:
			return guild_id

	def ReadYaml(self, path: Path):
		if path.exists():
			with open(path, 'r') as stream:
				return yaml.safe_load(stream)
		else:
			return

	def WriteYaml(self, path: Path, data: dict):
		log.debug(f'WriteYaml {path}')
		if not path.parent.exists():
			path.parent.mkdir(parents=True, exist_ok=True)
		with open(path, 'w+') as stream:
			yaml.dump(data, stream)

	def GetYamlName(self, file) -> str:
		file = str(file).lower()
		if not file.endswith('.yaml'):
			return file + '.yaml'
		return file

	def ValidFileName(self, name: str):
		# Define a regular expression pattern to match forbidden characters
		ILLEGAL_NTFS_CHARS = r'[<>:/\\|?*\"]|[\0-\31]'
		# Define a list of forbidden names
		FORBIDDEN_NAMES = [
			'CON', 'PRN', 'AUX', 'NUL',
			'COM1', 'COM2', 'COM3', 'COM4', 'COM5',
			'COM6', 'COM7', 'COM8', 'COM9',
			'LPT1', 'LPT2', 'LPT3', 'LPT4', 'LPT5',
			'LPT6', 'LPT7', 'LPT8', 'LPT9'
		]
		# Check for forbidden characters
		match = re.search(ILLEGAL_NTFS_CHARS, name)
		if match:
			raise ValueError(
			f"Invalid character '{match[0]}' for filename {name}")
		# Check for forbidden names
		if name.upper() in FORBIDDEN_NAMES:
			raise ValueError(f"{name} is a reserved folder name in windows")
		# Check for empty name (disallowed in Windows)
		if name.strip() == "":
			raise ValueError("Empty file name not allowed in Windows")
			# Check for names starting or ending with dot or space
			match = re.match(r'^[. ]|.*[. ]$', name)
		if match:
			raise ValueError(
				f"Invalid start or end character ('{match[0]}')"
				f" in file name {name}"
			)

	def ReadData(self, guild, file: str):
		guild_id = self.GetGuild(guild)
		if guild_id != 0:
			path = self.dataPath / str(guild_id) / self.GetYamlName(file)
			log.debug(f'Reading data from {path}')
			return self.ReadYaml(path)
		else:
			return

	def WriteData(self, guild, file: str | int, data: dict):
		file = str(file)
		guild_id = self.GetGuild(guild)
		if guild_id != 0:
			try:
				self.ValidFileName(self.GetYamlName(file))
			except ValueError as e:
				log.critical(str(e))
				return
			path = self.dataPath / str(guild_id) / self.GetYamlName(file)
			log.debug(f'Writing data to {path}')
			self.WriteYaml(path, data)
			return True

	def DeleteData(self, guild, file: str | int):
		file = str(file)
		guild_id = self.GetGuild(guild)
		filename = self.GetYamlName(file)
		try:
			self.ValidFileName(self.GetYamlName(filename))
		except ValueError as e:
			print('invalid filename')
			return
		path = self.dataPath / str(guild_id) / filename
		if path.exists():
			log.debug(f'Deleting data at {str(path)}')
			path.unlink(missing_ok=True)
			return True

	def TouchData(self, guild, file: str | int):
		file = str(file)
		guild_id = self.GetGuild(guild)
		if guild_id != 0:
			try:
				self.ValidFileName(file)
			except ValueError as e:
				log.critical(str(e))
				return
			path = self.dataPath / str(guild_id) / file
			path.touch(exist_ok=True)
			return True

	def DeleteAttachment(self, file: str):
		return self.AttachmentHandler.DeleteAttachment(file)

	async def SaveAttachment(self, module: str, attachment: discord.Attachment):
		ext = ''.join(Path(attachment.filename).suffixes) # collect the full extension (including .tar.gz etc - thanks stackoverflow)
		h = blake2b(digest_size=12)
		h.update(attachment.url.encode()) # encode the url to a 16-character string as a unique file identifier
		path = self.bot.AttachmentPath
		finalname = h.hexdigest() + ext
		try:
			self.ValidFileName(self.GetYamlName(finalname))
		except ValueError as e:
			return
		filepath = path / finalname
		path.mkdir(parents=True, exist_ok=True)
		log.debug(f'Saving attachment as {str(filepath)}')
		await attachment.save(filepath, use_cached=True)
		self.AttachmentHandler.RegisterAttachment(module, finalname)
		return str(finalname)

	def GetAttachmentPath(self, file: str):
		return self.bot.AttachmentPath / file

	def GetDataPath(self, guild: int | discord.Guild | discord.ext.commands.Context):
		guild_id = self.GetGuild(guild)
		if guild_id != 0:
			return self.dataPath / str(guild_id)

	async def GetEffectiveUserLevel(self, channel, user):
		cache = self.bot.Cache
		user_level = cache.GetUserLevel(channel, user)
		role_level = cache.UserLevels['User']
		try:
			guild = await self.bot.fetch_guild(int(channel))
			try:
				member = await guild.fetch_member(int(user))
				for role in member.roles:
					role_level = min(cache.GetRoleLevel(channel, role.id), role_level)
			except Exception as e:
				return cache.UserLevels['User']
		except Forbidden as e:
			return cache.UserLevels['User']
		return min(user_level, role_level)

	async def IsCommandPermitted(self, channel, user, targetLevel: str):
		cache = self.bot.Cache
		if targetLevel not in cache.UserLevels:
			log.debug(f'User {user} in channel {channel} has no permissions.')
			return
		target = cache.UserLevels[targetLevel]
		userLevel = await self.GetEffectiveUserLevel(channel, user)
		log.debug(f'User {user} is permission level {userLevel}, authenticating against level {target}')
		if userLevel is None:
			return
		else:
			return userLevel <= target

	def IsCommandDisabled(self, command: str, context):
		cache = self.bot.DisabledCommands
		command = command.lower()
		channel = self.GetGuild(context)
		if command in cache:
			if channel in cache[command]:
				return True

	def DisableCommand(self, command: str, context):
		cache = self.bot.DisabledCommands
		command = command.lower()
		channel = self.GetGuild(context)
		if command not in cache:
			cache[command] = {}
		cache[command][channel] = True
		self.bot.SaveDisabledCommands()

	def EnableCommand(self, command: str, context):
		cache = self.bot.DisabledCommands
		command = command.lower()
		channel = self.GetGuild(context)
		if command in cache:
			cache[command].pop(command, None)
			self.bot.SaveDisabledCommands()

	def GetDataList(self, guild) -> list:
		path = self.GetDataPath(guild)
		file_list = list(path.glob('*.yaml'))
		output_list = []
		for file in file_list:
			try:
				output_list.append(int(file.stem))
			except:
				output_list.append(file.stem)
		return output_list

# Transparent variant of ChatModule that caches all IO. Should be slightly faster than standard ChatModule but will use more memory
class CachedChatModule(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self._cache = {}

	def InitData(self, guild):
		guild_id = self.GetGuild(guild)
		if guild_id not in self._cache:
			self._cache[guild_id] = {
				'config': {},
				'list': super().GetDataList(guild_id)
			}

	def ReadData(self, guild, file):
		guild_id = self.GetGuild(guild)
		fname = str(file)
		self.InitData(guild)
		if fname not in self._cache[guild_id]['config']:
			data = super().ReadData(guild, fname)
			self._cache[guild_id]['config'][fname] = data
			return data
		return self._cache[guild_id]['config'][fname]

	def WriteData(self, guild, file, data: dict):
		guild_id = self.GetGuild(guild)
		fname = str(file)
		self.InitData(guild)
		self._cache[guild_id]['config'][fname] = data
		super().WriteData(guild, fname, data)
		# avoid disk IO if not required
		if fname.lower() not in self._cache[guild_id]['list']:
			self._cache[guild_id]['list'] = self.GetDataList(guild_id)
		return True

	def GetDataList(self, guild) -> list:
		guild_id = self.GetGuild(guild)
		self.InitData(guild)
		return self._cache[guild_id]['list']