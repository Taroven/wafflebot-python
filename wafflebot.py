import logging
import sys
import os
import pathlib
from pathlib import Path
import yaml
import discord
import re
from discord.ext import commands
from Util.ChannelCache import ChannelCache

discord.utils.setup_logging(level=logging.INFO, root=True)
log = logging.getLogger('WaffleBot')
logging.basicConfig(filename='wafflebot.log', encoding='utf-8', level=logging.DEBUG)

class WaffleBot(commands.Bot):
	def __init__(self, **kwargs):
		x = dict(command_prefix = kwargs['command_prefix'], intents = kwargs['intents'])
		super().__init__(**x)
		self.operator = kwargs.pop('operator', None)
		self.wwwAddress = kwargs.pop('www', None)
		self.infoAddress = kwargs.pop('info', None)
		self.ForceSync = kwargs.pop('sync', None)
		self.wwwAttachmentPath = kwargs.pop('short', 'Attachments')
		self.BaseConfigPath = Path.home() / '.config' / 'WaffleBot'
		self.GlobalDataPath = self.BaseConfigPath / 'GlobalData'
		self.AttachmentPath = self.BaseConfigPath / 'Attachments'
		self.DisabledCommands = self.GetDisabledCommands()
		self.Extensions = [
			'operator',
			'permissions',
			'quotes',
			'definitions',
			'translator',
			'timers',
			'random',
			'roles',
			'variables',
			'info',
			'submissions',
			'gambling',
		]

	async def on_ready(self):
		log.info(f'Logged in as {self.user} (ID: {self.user.id})')
		if self.ForceSync is not None:
			commands = await self.tree.sync()
			log.warn(f"Synced {len(commands)} commands to all connected guilds - Remove sync from the global config and restart")

	async def setup_hook(self):
		log.info('WaffleBot starting.')
		self.Cache = ChannelCache()

		# Load up our cogs
		for ext in self.Extensions:
			await self.load_extension(f'DiscordModules.{ext}')

	async def LoadExtension(self, ext: str):
		ext = ext.strip().lower()
		if ext not in self.Extensions:
			log.warn(f'Loading module {ext}. Crash probably impending.')
			self.Extensions.append(ext)
			await self.load_extension(f'DiscordModules.{ext}')

	async def ReloadExtension(self, ext: str):
		ext = ext.strip().lower()
		if ext in self.Extensions:
			log.warn(f'Reloading module {ext}. Crash probably impending.')
			await self.reload_extension(f'DiscordModules.{ext}')
			return True

	def ReadYaml(self, path: Path):
		if path.exists():
			with open(path, 'r') as stream:
				return yaml.safe_load(stream)
		else:
			return

	def WriteYaml(self, path: Path, data: dict):
		log.debug(f'WriteYaml {path}')
		if not path.parent.exists():
			path.parent.mkdir(parents=True, exist_ok=True)
		with open(path, 'w+') as stream:
			yaml.dump(data, stream)

	def GetYamlName(self, file: str) -> str:
		if not file.endswith('.yaml'):
			return file + '.yaml'
		return file

	def ValidFileName(self, name: str):
		# Define a regular expression pattern to match forbidden characters
		ILLEGAL_NTFS_CHARS = r'[<>:/\\|?*\"]|[\0-\31]'
		# Define a list of forbidden names
		FORBIDDEN_NAMES = [
			'CON', 'PRN', 'AUX', 'NUL',
			'COM1', 'COM2', 'COM3', 'COM4', 'COM5',
			'COM6', 'COM7', 'COM8', 'COM9',
			'LPT1', 'LPT2', 'LPT3', 'LPT4', 'LPT5',
			'LPT6', 'LPT7', 'LPT8', 'LPT9'
		]
		# Check for forbidden characters
		match = re.search(ILLEGAL_NTFS_CHARS, name)
		if match:
			raise ValueError(
			f"Invalid character '{match[0]}' for filename {name}")
		# Check for forbidden names
		if name.upper() in FORBIDDEN_NAMES:
			raise ValueError(f"{name} is a reserved folder name in windows")
		# Check for empty name (disallowed in Windows)
		if name.strip() == "":
			raise ValueError("Empty file name not allowed in Windows")
			# Check for names starting or ending with dot or space
			match = re.match(r'^[. ]|.*[. ]$', name)
		if match:
			raise ValueError(
				f"Invalid start or end character ('{match[0]}')"
				f" in file name {name}"
			)

	def ReadGlobalData(self, file: str):	
		path = self.GlobalDataPath / self.GetYamlName(file)
		log.debug(f'Reading global data from {path}')
		return self.ReadYaml(path)

	def WriteGlobalData(self, file: str | int, data: dict):
		file = str(file)
		try:
			self.ValidFileName(self.GetYamlName(file))
		except ValueError as e:
			log.critical(str(e))
			return
		path = self.GlobalDataPath / self.GetYamlName(file)
		log.debug(f'Writing global data to {path}')
		self.WriteYaml(path, data)
		return True

	def DeleteGlobalData(self, file: str):
		filename = self.GetYamlName(file)
		try:
			self.ValidFileName(self.GetYamlName(filename))
		except ValueError as e:
			print('invalid filename')
			return
		path = self.GlobalDataPath / filename
		if path.exists():
			log.debug(f'Deleting global data at {str(path)}')
			path.unlink(missing_ok=True)
			return True

	def GetDisabledCommands(self):
		data = self.ReadGlobalData('DisabledCommands')
		if data is None:
			data = {}
		return data

	def SaveDisabledCommands(self):
		self.WriteGlobalData('DisabledCommands', self.DisabledCommands)

def StartDiscord():
	DiscordConfig = {}
	discord_config_path = Path.home() / '.config' / 'WaffleBot'
	discord_config_file = discord_config_path / 'discord.yaml'
	discord_default_token = 'YOUR_DISCORD_BOT_TOKEN_HERE'
	discord_default_config = {
		'token': discord_default_token,
		'operator': 123456789,
		'www': 'https://www.example.com',
		'short': 'a',
		'info': 'info.html?g='
	}

	DiscordIntents = discord.Intents.default()
	DiscordIntents.message_content = True
	DiscordIntents.members = True
	
	# Create the config file if needed
	if not discord_config_file.exists():
		discord_config_path.mkdir(parents=True, exist_ok=True)
		with open(discord_config_file, 'w+') as stream:
			yaml.dump(discord_default_config, stream)

	with open(discord_config_file, 'r') as stream:
		DiscordConfig = yaml.safe_load(stream)
	if DiscordConfig['token'] == discord_default_token:
		print('Your Discord bot token is not set. Please edit ' + str(discord_config_file) + ' to connect. (You may need to add quotes around the token field)')
		print('The "operator" field should be your user ID. Without it set properly you cannot use #op commands.')
		print('The "www" field is used for a local webserver to send attachment permalinks in chat. If you do not run a webserver, delete the line.')
		print('The "short" field is used to change the directory within the web address used for attachments for purposes of shorter links.')
		return

	command_prefix = []
	if 'prefixes' in DiscordConfig:
		if 'when_mentioned' in DiscordConfig:
			if DiscordConfig['when_mentioned']:
				command_prefix = commands.when_mentioned_or(*DiscordConfig['prefixes'])
			else:
				command_prefix = DiscordConfig['prefixes']
	sync = None
	info = None
	if 'sync' in DiscordConfig:
		sync = True
	if 'info' in DiscordConfig and 'www' in DiscordConfig:
		info = DiscordConfig['info']
	
	bot = WaffleBot(command_prefix=command_prefix, intents=DiscordIntents, operator=DiscordConfig['operator'], www=DiscordConfig['www'], short=DiscordConfig['short'], sync=sync, info=info)
	bot.run(DiscordConfig['token'], log_handler=None)

StartDiscord()