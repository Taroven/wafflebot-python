import os
from hashlib import blake2b
import pathlib
from pathlib import Path
import yaml
import discord
import re
import logging
from typing import Any, List, Union
from hashlib import blake2b
import twitchio
from twitchio.ext import commands
from urllib.parse import urljoin

from Util.Attachments import AttachmentHandler

log = logging.getLogger('TwitchUtil.ChatModule')

class ChatModule(commands.Cog):
	def __init__(self, bot):
		super().__init__()
		self.bot = bot
		self.AttachmentHandler = AttachmentHandler(bot)
		self.dataPath = Path.home() / '.config' / 'WaffleBot' / self.name
		self.dataPath.mkdir(parents=True, exist_ok=True)
		self.Initialized = {}
		log.info(f'{self.name} initialized')

	def GetGuild(self, context = str | twitchio.Message | commands.Context | twitchio.Channel) -> int:
		channel = None
		if isinstance(context, str):
			channel = context
		elif isinstance(context, twitchio.Message) or isinstance(context, commands.Context):
			channel = context.channel.name
		elif isinstance(context, twitchio.Channel):
			channel = context.name
		if channel != None:
			channel = channel.lower()
		if channel != None and channel in self.bot.ChannelMap:
			return self.bot.ChannelMap[channel]

	def ReadYaml(self, path: Path):
		if path.exists():
			with open(path, 'r') as stream:
				return yaml.safe_load(stream)
		else:
			return

	def WriteYaml(self, path: Path, data: dict):
		log.debug(f'WriteYaml {path}')
		if not path.parent.exists():
			path.parent.mkdir(parents=True, exist_ok=True)
		with open(path, 'w+') as stream:
			yaml.dump(data, stream)

	def GetYamlName(self, file) -> str:
		file = str(file).lower()
		if not file.endswith('.yaml'):
			return file + '.yaml'
		return file

	def ValidFileName(self, name: str):
		# Define a regular expression pattern to match forbidden characters
		ILLEGAL_NTFS_CHARS = r'[<>:/\\|?*\"]|[\0-\31]'
		# Define a list of forbidden names
		FORBIDDEN_NAMES = [
			'CON', 'PRN', 'AUX', 'NUL',
			'COM1', 'COM2', 'COM3', 'COM4', 'COM5',
			'COM6', 'COM7', 'COM8', 'COM9',
			'LPT1', 'LPT2', 'LPT3', 'LPT4', 'LPT5',
			'LPT6', 'LPT7', 'LPT8', 'LPT9'
		]
		# Check for forbidden characters
		match = re.search(ILLEGAL_NTFS_CHARS, name)
		if match:
			raise ValueError(
			f"Invalid character '{match[0]}' for filename {name}")
		# Check for forbidden names
		if name.upper() in FORBIDDEN_NAMES:
			raise ValueError(f"{name} is a reserved folder name in windows")
		# Check for empty name (disallowed in Windows)
		if name.strip() == "":
			raise ValueError("Empty file name not allowed in Windows")
			# Check for names starting or ending with dot or space
			match = re.match(r'^[. ]|.*[. ]$', name)
		if match:
			raise ValueError(
				f"Invalid start or end character ('{match[0]}')"
				f" in file name {name}"
			)

	def ReadData(self, channel, file: str):
		guild_id = self.GetGuild(channel)
		if guild_id != 0:
			path = self.dataPath / str(guild_id) / self.GetYamlName(file)
			log.debug(f'Reading data from {path}')
			return self.ReadYaml(path)
		else:
			return

	def WriteData(self, channel, file: str | int, data: dict):
		file = str(file)
		guild_id = self.GetGuild(channel)
		if guild_id != 0:
			try:
				self.ValidFileName(self.GetYamlName(file))
			except ValueError as e:
				log.critical(str(e))
				return
			path = self.dataPath / str(guild_id) / self.GetYamlName(file)
			log.debug(f'Writing data to {path}')
			self.WriteYaml(path, data)
			return True

	def DeleteData(self, channel, file: str | int):
		file = str(file)
		guild_id = self.GetGuild(channel)
		filename = self.GetYamlName(file)
		try:
			self.ValidFileName(self.GetYamlName(filename))
		except ValueError as e:
			print('invalid filename')
			return
		path = self.dataPath / str(guild_id) / filename
		if path.exists():
			log.debug(f'Deleting data at {str(path)}')
			path.unlink(missing_ok=True)
			return True

	def DeleteAttachment(self, file: str):
		return self.AttachmentHandler.DeleteAttachment(file)

	def GetAttachmentPath(self, file: str):
		return self.bot.AttachmentPath / file

	# TODO: Links are long. Shorten them.
	def GetAttachmentAddress(self, file: str):
		return self.AttachmentHandler.GetAttachmentAddress(file)

	def GetDataPath(self, channel):
		guild_id = self.GetGuild(channel)
		if guild_id != 0:
			return self.dataPath / str(guild_id)

	def GetEffectiveUserLevel(self, channel: twitchio.Channel | twitchio.ext.commands.Context | str, user):
		c = channel
		if isinstance(channel, twitchio.Channel):
			c = channel.name.lower()
		elif isinstance(channel, twitchio.ext.commands.Context):
			c = channel.channel.name.lower()
		else:
			c = c.lower()
		return self.bot.Cache.GetUserLevel(c, user)

	def IsCommandPermitted(self, channel, user, targetLevel: str):
		cache = self.bot.Cache
		if targetLevel not in cache.UserLevels:
			log.debug(f'User {user} in channel {channel} has no permissions.')
			return
		target = cache.UserLevels[targetLevel]
		userLevel = self.GetEffectiveUserLevel(channel, user)
		log.debug(f'User {user} is permission level {userLevel}, authenticating against level {target}')
		if userLevel is None:
			return
		else:
			return userLevel <= target

	def IsCommandDisabled(self, command: str, context):
		cache = self.bot.DisabledCommands
		command = command.lower()
		channel = self.GetGuild(context)
		if command in cache:
			if channel in cache[command]:
				return True

	def DisableCommand(self, command: str, context):
		cache = self.bot.DisabledCommands
		command = command.lower()
		channel = self.GetGuild(context)
		if command not in cache:
			cache[command] = {}
		cache[command][channel] = True
		self.bot.SaveDisabledCommands()

	def EnableCommand(self, command: str, context):
		cache = self.bot.DisabledCommands
		command = command.lower()
		channel = self.GetGuild(context)
		if command in cache:
			cache[command].pop(command, None)
			self.bot.SaveDisabledCommands()

	def AddCommands(self, *args):
		for arg in args:
			self.bot.AddCommand(self.name, arg)

	def GetDataList(self, channel) -> list:
		path = self.GetDataPath(channel)
		file_list = list(path.glob('*.yaml'))
		output_list = []
		for file in file_list:
			try:
				output_list.append(int(file.stem.lower()))
			except:
				pass
		return output_list

# Transparent variant of ChatModule that caches all IO. Should be slightly faster than standard ChatModule but will use more memory
class CachedChatModule(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self._cache = {}

	def InitData(self, channel):
		guild_id = self.GetGuild(channel)
		if guild_id not in self._cache:
			self._cache[guild_id] = {
				'config': {},
				'list': super().GetDataList(guild_id)
			}

	def ReadData(self, channel, file):
		guild_id = self.GetGuild(channel)
		fname = str(file)
		self.InitData(channel)
		if fname not in self._cache[guild_id]['config']:
			data = super().ReadData(channel, fname)
			self._cache[guild_id]['config'][fname] = data
			return data
		return self._cache[guild_id]['config'][fname]

	def WriteData(self, channel, file, data: dict):
		guild_id = self.GetGuild(channel)
		fname = str(file)
		self.InitData(channel)
		self._cache[guild_id]['config'][fname] = data
		super().WriteData(channel, fname, data)
		# avoid disk IO if not required
		if fname.lower() not in self._cache[guild_id]['list']:
			self._cache[guild_id]['list'] = self.GetDataList(guild_id)
		return True

	def GetDataList(self, channel) -> list:
		guild_id = self.GetGuild(channel)
		self.InitData(channel)
		return self._cache[guild_id]['list']