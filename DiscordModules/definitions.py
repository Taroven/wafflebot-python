import discord
from discord.ext import commands
from discord import app_commands
from typing import Optional
import re
import mimetypes
import logging
import json

from Util.APIParse import APIRequest
from Util.Substring import Substring
from DiscordUtil.ChatModule import ChatModule

log = logging.getLogger('WaffleBot.definitions')
never_mention = discord.AllowedMentions(everyone = False, users = False, roles = False, replied_user = False)
max_size = 25 * 1000 * 1000

# save a few cycles
Def = re.compile(r"^[\?\!]\s?(\w+)")

class Definitions(ChatModule):
	def __init__(self,bot):
		super().__init__(bot)
		self.Commands = {}
		self.API = APIRequest()
	
	def Delete(self, guild, word):
		data = self.ReadData(guild, word)
		if data != None:
			if 'attachment' in data:
				self.AttachmentHandler.UnregisterAttachment('Definitions', data['attachment'])
			return self.DeleteData(guild, word)

	@app_commands.command(name='define', description='Define a [word] to be returned by ?[word].')
	@app_commands.describe(
		word='Word to define',
		definition='Definition of the word - Use a semicolon (;) to add new lines. (optional if an attachment exists)',
		attachment='Attachment to embed with the definition (optional if a definition exists)',
		api='API to request (Optional - see documentation)',
		params='Parameters to send to the API (Optional - see documentation)'
	)
	async def define(self, interaction: discord.Interaction, word: str, definition: Optional[str], attachment: Optional[discord.Attachment], api: Optional[str], params: Optional[str]):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		if not word.isalnum():
			await interaction.response.send_message(f'`{word}` cannot be used as a definition.', ephemeral=True)
			return

		word = word.lower().strip()
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await interaction.response.send_message(f'`{word}` cannot be used as a definition.', ephemeral=True)
			return

		finaldef = {
			'author': interaction.user.global_name,
			'word': word,
		}
		if params != None:
			params = re.split(r'\s?;\s?')
			paramsDict = {}
			for key in params:
				temp = re.split(r'\s?=\s?')
				if len(temp) > 1:
					paramsDict[temp[0]] = temp[1]
			if len(paramsDict) > 0:
				finaldef['params'] = paramsDict
		if api != None and definition is None:
			await interaction.response.send_message('API requests require text content.')
			return
		if attachment is None and definition is None:
			await interaction.response.send_message(f'A definition or attachment is required to define `{word}`.', ephemeral=True)	
			return
		attachment_too_large = False
		if attachment != None:
			filename = await self.SaveAttachment('Definitions', attachment)
			attachment_path = self.GetAttachmentPath(filename)
			if attachment_path.exists():
				if attachment_path.stat().st_size > max_size:
					attachment_too_large = True
					self.AttachmentHandler.UnregisterAttachment('Definitions', filename)
				else:
					finaldef['attachment'] = filename
		if definition != None:
			definition = re.sub(r'\s?;\s?', '\n', definition)
			finaldef['content'] = definition
		if api != None:
			finaldef['api'] = api.strip()
			finaldef['fields'] = re.findall(r'\$(\S+)', definition)
			if len(finaldef['fields']) == 0:
				await interaction.response.send_message(f'Failed to define `{word}` - API requests must request content within the definition.\nFor example, to get the `content` field of the response, use `$content` in the definition. The tag will be replaced with its contents if possible.')
				return
		if attachment_too_large:
			await interaction.response.send_message(f'Failed to define `{word}` - Attachment too large.')
		else:
			self.WriteData(interaction.guild_id, word, finaldef)
			await interaction.response.send_message(f'Definition for `{word}` added.', ephemeral=True)

	@app_commands.command(name='undefine', description='Remove a word from definitions.')
	@app_commands.describe(
		word='Word to delete'
	)
	async def undefine(self, interaction: discord.Interaction, word: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		word = word.lower()
		if not word.isalnum():
			await interaction.response.send_message(f'`{word}` cannot be used as a definition.', ephemeral=True)
			return
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await interaction.response.send_message(f'`{word}` cannot be used as a definition.', ephemeral=True)
			return
		deleted = self.Delete(interaction.guild_id, word)
		if deleted == True:
			await interaction.response.send_message(f'Definition for `{word}` deleted.', ephemeral=True)
		else:
			await interaction.response.send_message(f'No definition for `{word}` found.', ephemeral=True)

	@app_commands.command(name='tocommand', description='Allow a definition to be used as a !command')
	@app_commands.describe(
		word='Definition to add to the !command list (?word may still be used)'
	)
	async def tocommand(self, interaction: discord.Interaction, word: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		word = word.lower()
		if not word.isalnum():
			await interaction.response.send_message(f'`{word}` cannot be used as a definition.', ephemeral=True)
			return
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await interaction.response.send_message(f'`{word}` cannot be used as a definition.', ephemeral=True)
			return
		data = self.ReadData(interaction.guild_id, '.commands')
		if data is None:
			data = {}
		data[word] = True
		self.WriteData(interaction.guild_id, '.commands', data)
		self.ReadCommands(interaction.guild)
		await interaction.response.send_message(f'`?{word}` may now be used as `!{word}.`', ephemeral=True)
		
	@app_commands.command(name='uncommand', description='Remove a ?definition as a !command')
	@app_commands.describe(
		word='Definition to remove from !command list (?word may still be used)'
	)
	async def uncommand(self, interaction: discord.Interaction, word: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		word = word.lower()
		if not word.isalnum():
			await interaction.response.send_message(f'`{word}` cannot be used as a definition.', ephemeral=True)
			return
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await interaction.response.send_message(f'`{word}` cannot be used as a definition.', ephemeral=True)
			return
		data = self.ReadData(interaction.guild_id, '.commands')
		data.pop(word, None)
		self.WriteData(interaction.guild_id, '.commands', data)
		await interaction.response.send_message(f'Removed `!{word}` (`?{word}` still exists)', ephemeral=True)

	@app_commands.command(name='alias', description='Link a word to an existing definition')
	@app_commands.describe(
		word='Definition to alias',
		newword='Word to alias to'
	)
	async def alias(self, interaction: discord.Interaction, word: str, newword: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		word = word.lower().strip()
		newword = newword.lower().strip()
		if not word.isalnum() or not newword.isalnum():
			await interaction.response.send_message(f'`{word}` and `{newword}` must be alphanumeric.', ephemeral=True)
			return
		try:
			self.ValidFileName(self.GetYamlName(newword))
		except ValueError:
			await interaction.response.send_message(f'`{newword}` cannot be used as a definition.', ephemeral=True)
			return
		data = {'redirect': word}
		self.WriteData(interaction.guild_id, newword, data)
		await interaction.response.send_message(f'Defined `?{newword}` as an alias of `?{word}`.', ephemeral=True)

	@app_commands.command(name='apitest', description='Get a full API response (useful for testing requests for /define)')
	@app_commands.describe(
		url='Definition to alias',
		params='Word to alias to'
	)
	async def apitest(self, interaction: discord.Interaction, url: str, params: Optional[str]):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Voice')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		url = url.strip()
		paramsDict = {}
		if params != None:
			params = re.split(r'\s?;\s?')
			for key in params:
				temp = re.split(r'\s?=\s?')
				if len(temp) > 1:
					paramsDict[temp[0]] = temp[1]
		response = await self.API.GetAllFromAPI(url, paramsDict)
		if isinstance(response, str):
			await interaction.response.send_message(response, ephemeral=True)
		elif isinstance(response, dict):
			pretty = json.dumps(response, indent=2)
			codified = '```json\n' + pretty + '```'
			await interaction.response.send_message(codified, ephemeral=True)


	@commands.Cog.listener()
	async def on_message(self, message):
		if message.author.id == self.bot.user.id:
			return
		result = Def.search(message.content)
		if result is None: #silent fail for messages like "???" or "?!"
			return
		else:
			word = result.group(1).lower()
			if message.content.startswith('!'):
				if word not in self.Commands[message.guild.id]:
					return
			if not word.isalnum(): #silent fail if we're not passing alphanumeric
				return
			definition = self.ReadData(message.guild, word)
			if definition is None: #silent fail if we don't have an existing definition
				return
			if 'redirect' in definition:
				definition = self.ReadData(message.guild, definition['redirect'])
				if definition is None:
					return

			content = None
			if 'api' in definition:
				content = await self.API.BuildFromAPI(definition)
				content = str(Substring(message, content))
			elif 'content' in definition:
				content = definition['content']
				content = str(Substring(message, content))
			if 'attachment' in definition:
				attach_path = self.GetAttachmentPath(definition['attachment'])
				if attach_path.exists():
					mimetype, irrel = mimetypes.guess_type(attach_path)
					file = discord.File(attach_path, filename = definition['attachment'])
					if mimetype.startswith('image'): # We can do some special sauce to not include file info, only the embed itself
						embed = discord.Embed()
						embed.set_image(url='attachment://' + definition['attachment'])
						await message.reply(content=content, file=file, embed=embed, allowed_mentions=never_mention)
					else:
						await message.reply(content=content, file=file, allowed_mentions=never_mention)
			else:
				await message.reply(content=content, silent=True, allowed_mentions=never_mention)

	def ReadCommands(self, guild):
		commands = self.ReadData(guild, '.commands')
		if commands is None:
			commands = {}
		self.Commands[guild.id] = commands

	@commands.Cog.listener()
	async def on_ready(self):
		log.info(f'{self.qualified_name} loaded and ostensibly ready')
		for guild in self.bot.guilds:
			self.ReadCommands(guild)

	@commands.Cog.listener()
	async def on_guild_join(self, guild):
		self.ReadCommands(guild)

	@commands.Cog.listener()
	async def on_guild_available(self, guild):
		self.ReadCommands(guild)

async def setup(bot):
	await bot.add_cog(Definitions(bot))