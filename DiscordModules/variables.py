import discord
from discord.ext import commands
from discord import app_commands
from typing import Optional
import re
import mimetypes
import logging
import json

from Util.APIParse import APIRequest
from Util.Substring import Substring
from DiscordUtil.ChatModule import ChatModule

log = logging.getLogger('WaffleBot.variables')

# save a few cycles
Print = re.compile(r"^[=#]\s?(\w+)")
Increment = re.compile(r"^(\w+)\s?([\+\-]{2})")
Set = re.compile(r"^(\w+)\s?([\+\-=])\s?(\d+)")

class Variables(ChatModule):
	@app_commands.command(name='addvariable', description='Add a variable (counter) to keep track of')
	@app_commands.describe(
		name='Label of the variable (counter)',
		alias='(optional) Override name when printed in chat',
		initial='(optional) Starting value - Default 0'
	)
	async def addvariable(self, interaction: discord.Interaction, name: str, alias: Optional[str], initial: Optional[int]):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Op')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		if not name.isalnum():
			await interaction.response.send_message(f'`{name}` cannot be used as a variable name.', ephemeral=True)
			return
		
		name = name.strip().lower()
		try:
			self.ValidFileName(self.GetYamlName(name))
		except ValueError:
			await interaction.response.send_message(f'`{name}` cannot be used as a variable name.', ephemeral=True)
			return
		
		data = {
			'val': 0 if initial is None else initial
		}
		output = f'Variable `{name}` created'
		if alias is not None:
			alias = alias.strip()
			output += f' with alias {alias}'
			data['alias'] = alias
		output += '.\n\nUse `{name}++` to increment by 1, `{name}--` to decrement by 1, `{name}+X` to add `X`, `{name}-X` to subtract `X`, or `{name}=X` to set to `X`.'

		self.WriteData(interaction.guild_id, name, data)
		await interaction.response.send_message(output, ephemeral=True)

	@app_commands.command(name='unvariable', description='Remove a variable (counter)')
	@app_commands.describe(
		name='Label of the variable (counter)'
	)
	async def unvariable(self, interaction: discord.Interaction, name: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Op')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		if not name.isalnum():
			await interaction.response.send_message(f'`{name}` cannot be used as a variable name.', ephemeral=True)
			return

		name = name.strip().lower()
		self.DeleteData(interaction.guild_id, name)
		await interaction.response.send_message(f'Variable {name} has been deleted.', ephemeral=True)

	@commands.Cog.listener()
	async def on_message(self, message):
		if message.author.id == self.bot.user.id:
			return
		isPrint = Print.search(message.content)
		if isPrint is not None:
			var = isPrint.group(1).lower()
			data = self.ReadData(message.guild.id, var)
			if data is not None:
				name = var if 'alias' not in data else data['alias']
				await message.reply(f'{name}: {data["val"]}')
				return

		permitted = await self.IsCommandPermitted(message.guild.id, message.author.id, 'Op')
		if not permitted:
			return

		isIncrement = Increment.search(message.content)
		if isIncrement is not None:	
			var = isIncrement.group(1).lower()
			data = self.ReadData(message.guild.id, var)
			if data is not None:
				name = var if 'alias' not in data else data['alias']
				if isIncrement.group(2) == '++':
					data["val"] += 1
				elif isIncrement.group(2) == '--':
					data["val"] -= 1
				self.WriteData(message.guild.id, var, data)
				await message.reply(f'{name}: {data["val"]}')
				return

		isSet = Set.search(message.content)
		if isSet is not None:
			var = isSet.group(1).lower()
			data = self.ReadData(message.guild.id, var)
			if data is not None:
				name = var if 'alias' not in data else data['alias']
				newval = int(isSet.group(3))
				if isSet.group(2) == '+':
					data['val'] += newval
				elif isSet.group(2) == '-':
					data["val"] -= newval
				elif isSet.group(2) == '=':
					data["val"] = newval
				self.WriteData(message.guild.id, var, data)
				await message.reply(f'{name}: {data["val"]}')
				return

async def setup(bot):
	await bot.add_cog(Variables(bot))