import discord
import re
import logging
import random
from typing import Any, List, Union, Optional
from discord.ext import commands
from discord.ui import RoleSelect
from discord import app_commands, ui, Interaction

from DiscordUtil.ChatModule import ChatModule

log = logging.getLogger('WaffleBot.roles')

class RolesSelect(RoleSelect):
	async def callback(self, interaction: discord.Interaction):
		roleData = {
			'alias': self.alias,
			'roles': []
		}
		selected_roles = [
			role.id
			for role in self.values
			if isinstance(role, discord.Role)
		]

		affected_roles = []
		roleList = {}

		for role in interaction.guild.roles:
			roleList[role.id] = role.mention
		for role_id in selected_roles:
			affected_roles.append(roleList[role_id])
			roleData['roles'].append(role_id)
		
		if len(affected_roles) > 0:
			self.parent.WriteData(self.view.message.guild, self.alias, roleData)
			role = affected_roles[0]
			await self.message.edit(content = f'Set alias `{self.alias}` to role {role}.', view = None)
		else:
			await self.message.edit(content = 'Something went wrong.', view = None)

class RolesView(discord.ui.View):
	def __init__(self, parent, alias):
		super().__init__()
		self.alias = alias
		self.parent = parent
		self.bot = parent.bot
		selector = RolesSelect(placeholder='Select a role.')
		selector.bot = self.bot
		selector.alias = alias
		selector.parent = parent
		self.selector = selector
		self.add_item( selector )

class Roles(ChatModule):
	def GetRoleList(self, guild):
		data_list = self.GetDataList(guild)
		if len(data_list) > 0:
			output = "Roles available for enrollment:"
			for role in data_list:
				data = self.ReadData(guild, role)
				output += f"\n- `{role}`"
			return output
		else:
			return "No roles have been set up yet. Please yell at your server owner."

	def GetRandomColor(self):
		r = random.randint(0,255)
		g = random.randint(0,255)
		b = random.randint(0,255)
		return discord.Color.from_rgb(r,g,b)

	def GetColor(self, code):
		try:
			return discord.Color.from_str(code)
		except:
			pass

	async def FindColorRole(self, guild, color):
		colorcode = str(color)
		for role in guild.roles:
			if role.name == str(color):
				return role
		try:
			role = await guild.create_role(name=str(color), color=color, reason='Waffles color-role')
			return role
		except:
			log.error(f'Could not create role {colorcode} in server {guild.id}')

	async def AssignColor(self, guild, user, color):
		colorcode = str(color)
		new_role = await self.FindColorRole(guild, color)
		if new_role is None:
			return
		for old_role in user.roles:
			if old_role.name.startswith('#'):
				try:
					await user.remove_roles(old_role)
					if len(old_role.members) == 0:
						await old_role.delete(reason='Waffles color-role (role is empty)')
				except:
					log.error(f'Could not remove role {old_role.name} from user {user.id}')
		try:
			await user.add_roles(new_role)
			return colorcode
		except:
			log.error(f'Could not add role {new_role.name} to user {user.id}')

	async def RemoveColor(self, user):
		for role in user.roles:
			if role.name.startswith('#'):
				try:
					await user.remove_roles(role)
					if len(role.members) == 0:
						await role.delete(reason='Waffles color-role (role is empty)')
					return True
				except:
					log.error(f'Could not remove role {role.name} from user {user.id}')

	@app_commands.command(name='addenrollment', description="Adds a chat command for users to add or remove themselves from a role.")
	@app_commands.describe(
		alias='Alias for the role as typed in chat (Must be alphanumeric)'
	)
	async def addenrollment(self, interaction: discord.Interaction, alias: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('Only administrators may use this command.', ephemeral = True)
			return
		alias = alias.strip().lower()
		if not alias.isalnum():
			await interaction.response.send_message('The alias must be alphanumeric (no spaces or special characters).', ephemeral = True)
			return
		view = RolesView(self, alias)
		await interaction.response.send_message(f'Select role to set for alias `{alias}`:', view = view, ephemeral = True)
		view.message = await interaction.original_response()
		view.selector.message = view.message

	@app_commands.command(name='removeenrollment', description="Removes a user enrollment.")
	@app_commands.describe(
		alias='Alias for the role as typed in chat (Must be alphanumeric)'
	)
	async def removeenrollment(self, interaction: discord.Interaction, alias: str):
		if not permitted:
			await interaction.response.send_message('Only administrators may use this command.', ephemeral = True)
			return
		alias = alias.strip().lower()
		if not alias.isalnum():
			await interaction.response.send_message('The alias must be alphanumeric (no spaces or special characters).', ephemeral = True)
			return
		self.DeleteData(interaction.guild_id, alias)
		await interaction.response.send_message('The enrollment alias `alias` has been deleted. Users who have enrolled still have the role if it exists.', ephemeral = True)

	@app_commands.command(name='enroll', description="Give yourself a chat role!")
	@app_commands.describe(
		role='Which roll to assign (invalid entries will reply with the roll list)'
	)
	async def enroll(self, interaction: discord.Interaction, role: str):
		role = role.strip().lower()
		if not role.isalnum():
			await interaction.response.send_message(self.GetRoleList(interaction.guild_id), ephemeral = True)
			return
		data = self.ReadData(interaction.guild_id, role)
		if data is None:
			await interaction.response.send_message(self.GetRoleList(interaction.guild_id), ephemeral = True)
			return
		else:
			roles = []
			for role_id in data['roles']:
				role = interaction.guild.get_role(role_id)
				if role is not None:
					roles.append(role)
			if len(roles) > 0:
				try:
					await interaction.user.add_roles(*roles)
					await interaction.response.send_message(f'You have been added to {roles[0].mention} - If you change your mind, use `/unenroll` to remove it.', ephemeral = True)
				except Exception as e:
					log.error('Error at %s', 'enroll', exc_info=e)
					await interaction.response.send_message('Something went wrong. Please contact the server owner and let them know.', ephemeral = True)

	@app_commands.command(name='unenroll', description="Remove an role assignment")
	@app_commands.describe(
		role='Which roll to remove (invalid entries will reply with the roll list)'
	)
	async def unenroll(self, interaction: discord.Interaction, role: str):
		role = role.strip().lower()
		if not role.isalnum():
			await interaction.response.send_message(self.GetRoleList(interaction.guild_id), ephemeral = True)
			return
		data = self.ReadData(interaction.guild_id, role)
		if data is None:
			await interaction.response.send_message(self.GetRoleList(interaction.guild_id), ephemeral = True)
			return
		else:
			roles = []
			for role_id in data['roles']:
				role = interaction.guild.get_role(role_id)
				if role is not None:
					roles.append(role)
			if len(roles) > 0:
				try:
					await interaction.user.remove_roles(*roles)
					await interaction.response.send_message(f'You have been removed from {roles[0].mention}', ephemeral = True)
				except Exception as e:
					log.error('Error at %s', 'unenroll', exc_info=e)
					await interaction.response.send_message('Something went wrong. Please contact the server owner and let them know.', ephemeral = True)

	@app_commands.command(name='color', description="Give yourself a fancy new color (or get rid of one, we don't judge)")
	@app_commands.describe(
		color='Color code in hex or rgb(r,g,b) format, clear, or random',
	)
	async def color(self, interaction: discord.Interaction, color: str):
		if self.IsCommandDisabled('color', interaction.guild_id):
			await interaction.response.send_message('This command has been disabled by your server owner.')
			return
		data = self.ReadData(interaction.guild_id, 'colors')
		color = color.strip().lower()
		if color == 'clear':
			success = await self.RemoveColor(interaction.user)
			if success:
				await interaction.response.send_message('Your custom color has been removed.', ephemeral=True)
		elif color == 'random':
			success = await self.AssignColor(interaction.guild, interaction.user, self.GetRandomColor())
			if success is None:
				await interaction.response.send_message('Something went wrong with changing your roles. Please inform the server owner.', ephemeral=True)
			else:
				await interaction.response.send_message(f'Your new color is `{success}`.', ephemeral=True)
		else:
			role_color = self.GetColor(color)
			if role_color is None:
				await interaction.response.send_message(f'Sorry, could not parse `{color}` - According to the discord.py docs, the following formats are accepted:\n\n- `0x<hex>`\n- `#<hex>`\n- `0x#<hex>`\n- `rgb(<number>, <number>, <number>)`', ephemeral=True)
			else:
				success = await self.AssignColor(interaction.guild, interaction.user, role_color)
				if success is None:
					await interaction.response.send_message('Something went wrong with changing your roles. Please inform the server owner.')
				else:
					await interaction.response.send_message(f'Your new color is `{success}`.', ephemeral=True)

async def setup(bot):
	await bot.add_cog(Roles(bot))