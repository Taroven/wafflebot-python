import discord
from discord.ext import commands
from discord import app_commands
from urllib.parse import urljoin

from DiscordUtil.ChatModule import ChatModule

class Info(ChatModule):
	@app_commands.command(name='info', description='Get some basic server information')
	async def info(self, interaction: discord.Interaction):
		try:
			url = urljoin(self.bot.wwwAddress, '/' + self.bot.infoAddress + str(interaction.guild_id))
			await interaction.response.send_message(f'Server information may be found at the following link:\n\n{url}', ephemeral=True)
		except:
			await interaction.response.send_message('The bot operator has not enabled this functionality.', ephemeral=True)

async def setup(bot):
	await bot.add_cog(Info(bot))