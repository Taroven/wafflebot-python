import discord
from discord.ext import commands
from discord import app_commands
from typing import Optional
import re

from DiscordUtil.ChatModule import ChatModule

class Submissions(ChatModule):
	@app_commands.command(name='submit', description="Submit your content!")
	@app_commands.describe(
		content='Link or other text to submit'
	)
	async def submit(self, interaction: discord.Interaction, content: str):
		if len(content.strip()) < 8:
			await interaction.response.send_message('Submission too short.', ephemeral=True)
			return
		data = self.ReadData(interaction.guild_id, 'current')
		if data is None:
			await interaction.response.send_message('No submissions are available right now.', ephemeral=True)
			return
		data['submissions'].append([interaction.user.global_name, content.strip()])
		self.WriteData(interaction.guild_id, 'current', data)
		await interaction.response.send_message('Your content has been submitted. Use `/unsubmit` to undo if you made a mistake.', ephemeral=True)

	@app_commands.command(name='unsubmit', description="Remove your latest submission")
	async def unsubmit(self, interaction: discord.Interaction):
		data = self.ReadData(interaction.guild_id, 'current')
		if data is None:
			await interaction.response.send_message('No submissions are available right now.', ephemeral=True)
			return
		user = interaction.user.global_name
		for i, e in reversed(list(enumerate(data['submissions']))):
			if user in e:
				if i < data['i']:
					await interaction.response.send_message('Your submission has already been read, it cannot be removed.', ephemeral=True)
					return
				data['submissions'].remove(e)
				self.WriteData(interaction.guild_id, 'current', data)
				await interaction.response.send_message('Your latest submission has been removed.', ephemeral=True)
				return
		await interaction.response.send_message('You have no submissions to remove. Use `/submit` to make one.', ephemeral=True)

	@app_commands.command(name='opensubmissions', description="Open a list of submissions (Admin+)")
	@app_commands.describe(
		restore='Restore previous submissions list? (optional)'
	)
	async def opensubmissions(self, interaction: discord.Interaction, restore: Optional[str]):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		if restore is not None:
			previous = self.ReadData(interaction.guild_id, 'previous')
			if previous is None:
				await interaction.response.send_message('No submissions to restore. Run this command again without the `restore` option to open submissions.', ephemeral=True)
				return
			else:
				current = self.ReadData(interaction.guild_id, 'current')
				self.WriteData(interaction.guild_id, 'previous', current)
				self.WriteData(interaction.guild_id, 'current', previous)
				await interaction.response.send_message('The previous submissions have been restored. If this was a mistake, redo this command.', ephemeral=True)
				return
		data = self.ReadData(interaction.guild_id, 'current')
		if data is None:
			data = { 'i': 0, 'submissions':[] }
			self.WriteData(interaction.guild_id, 'current', data)
			await interaction.response.send_message('Submissions have been opened.', ephemeral=True)
		else:
			await interaction.response.send_message('Submissions are already open. If you would like to reset the list, run `/closesubmissions` first. Note that this WILL delete all pending submissions.')

	@app_commands.command(name='closesubmissions', description="Close the current submissions list (Admin+)")
	async def closesubmissions(self, interaction: discord.Interaction):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		current = self.ReadData(interaction.guild_id, 'current')
		if current is None:
			await interaction.response.send_message('There are no submissions to close.', ephemeral=True)
			return
		self.WriteData(interaction.guild_id, 'previous', current)
		self.DeleteData(interaction.guild_id, 'current')
		await interaction.response.send_message('Submissions have been closed. To undo this, run `/opensubmissions` with the `restore` option.', ephemeral=True)

	@app_commands.command(name='nextsub', description="Get the next submission (Admin+)")
	async def nextsub(self, interaction: discord.Interaction):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		data = self.ReadData(interaction.guild_id, 'current')
		if data is None:
			await interaction.response.send_message('There are no open submissions.')
			return
		if data['i'] > len(data['submissions']) - 1:
			await interaction.response.send_message('There are no more submissions in the pool.')
			return
		current = data['submissions'][data['i']]
		output = f'`Submission {data["i"] + 1} by {current[0]}:`\n{current[1]}'
		await interaction.response.send_message(output)
		data['i'] += 1
		self.WriteData(interaction.guild_id, 'current', data)

async def setup(bot):
	await bot.add_cog(Submissions(bot))