import discord
from discord.ext import commands
from discord import app_commands
from typing import Optional
import re
import logging

from DiscordUtil.ChatModule import ChatModule

log = logging.getLogger("WaffleBot.operator")
never_mention = discord.AllowedMentions(
    everyone=False, users=False, roles=False, replied_user=False
)

async def Sync(self, **kwargs):
    log.warn("Syncing commands. Don't do this too often.")
    interaction = kwargs['interaction']
    guild = kwargs['extra']
    if guild is not None:
        try:
            guild = int(kwargs['extra'])
            commands = await self.bot.tree.sync(guild=guild)
            await interaction.response.send_message(f"Synced `{len(commands)}` commands to guild `{guild}`.", ephemeral = True)
        except:
            await interaction.response.send_message(f"Invalid input `{guild}`", ephemeral = True)
    else:
        commands = await self.bot.tree.sync()
        await interaction.response.send_message(f"Synced {len(commands)} commands to all connected guilds.", ephemeral = True)

async def Playing(self, **kwargs):
    interaction = kwargs['interaction']
    data = self.bot.ReadGlobalData("operator")
    if data is None:
        data = {}
    data["playing"] = kwargs['extra']
    self.bot.WriteGlobalData("operator", data)
    playing = discord.Game(kwargs['extra'])
    await self.bot.change_presence(status=None, activity=playing)
    await interaction.response.send_message(f"I am now playing {kwargs['extra']}", ephemeral=True)

async def Permissions(self, **kwargs):
    interaction = kwargs['interaction']
    d = self.bot.Cache.AddChannel(interaction.guild.id, interaction.guild.owner_id)
    content = "Users:\n```\n"
    for user_id, level in d["users"].items():
        try:
            user = await self.bot.fetch_user(int(user_id))
            content += f"{user.global_name} = {level} ({self.bot.Cache.UserLevelNames[level]})\n"
        except Exception as e:
            log.exception('Error getting permissions')
            return
    content += "```\nRoles:\n```\n"
    for role_id, level in d["roles"].items():
        try:
            for role_obj in interaction.guild.roles:
                if int(role_id) == role_obj.id:
                    content += f"{role_obj.name} = {level} ({self.bot.Cache.UserLevelNames[level]})\n"
        except Exception as e:
            log.exception('Error setting permissions content')
            return
    content += "```"
    await interaction.response.send_message(content, ephemeral=True)

async def Gamble(self, **kwargs):
    ext = kwargs['extra']
    interaction = kwargs['interaction']
    data = self.bot.ReadGlobalData('gamble')
    if data is None:
        data = {}
    data[interaction.guild_id] = False if ext == 'show' else True
    self.bot.WriteGlobalData('gamble', data)
    status = 'hidden' if data[interaction.guild_id] else 'shown' # this is backwards because we're using it to directly set ephemeral=status
    await interaction.response.send_message(f'Gambling will now be {status}.', ephemeral=True)

async def Link(self, **kwargs):
    ext = kwargs['extra'].lower()
    interaction = kwargs['interaction']
    data = self.bot.ReadGlobalData("twitch")
    if data is None:
        data = {}

    # get rid of old data before adding new
    if interaction.guild.owner_id in data:
        log.warn(f"Removing Twitch link for user {interaction.guild.owner_id}")
        old = data.pop(data[interaction.guild.owner_id], None)
        if old is not None:
            data.pop(data[old["channel"]], None)
    if ext in data:
        await interaction.response.send_message(f"Channel {ext} is already linked.", ephemeral=True)
        return
    else:
        log.warn(
            f"Adding Twitch link for user {interaction.guild.owner_id} ({ext})"
        )
        data[ext] = {
            "guild_id": interaction.guild.id,
            "owner_id": interaction.guild.owner_id,
            "channel": ext,
        }
        data[interaction.guild.owner_id] = data[ext]
        self.bot.WriteGlobalData("twitch", data)
        await interaction.response.send_message(f"Channel {ext} has been linked to Twitch. Run `/operator unlink` to undo this.", ephemeral=True)

async def Unlink(self, **kwargs):
    interaction = kwargs['interaction']
    data = self.bot.ReadGlobalData("twitch")
    if interaction.guild.owner_id in data:
        log.warn(f"Removing Twitch link for user {interaction.guild.owner_id}")
        ext = data[interaction.guild.owner_id]["channel"]
        data.pop(data[interaction.guild.owner_id]["channel"], None)
        data.pop(interaction.guild.owner_id, None)
        self.bot.WriteGlobalData("twitch", data)
        await interaction.response.send_message(f"Channel {ext} unlinked from Twitch.", ephemeral=True)

async def Disable(self, **kwargs):
    command = kwargs['extra'].lower()
    interaction = kwargs['interaction']
    if self.DisableCommand(command, interaction.guild):
        await interaction.response.send_message(f"{command} has been disabled on this server.", ephemeral=True)
    else:
        await interaction.response.send_message(f"{command} cannot be disabled.", ephemeral=True)

async def Enable(self, **kwargs):
    command = kwargs['extra'].lower()
    interaction = kwargs['interaction']
    if self.EnableCommand(command, interaction.guild):
        await interaction.response.send_message(f"{command} has been enabled on this server.", ephemeral=True)
    else:
        await interaction.response.send_message(f"{command} cannot be disabled.", ephemeral=True)

class Operator(ChatModule):
    def __init__(self, bot):
        super().__init__(bot)
        self.Commands = {
            'operator': {
                'playing': Playing,
                'sync': Sync,
            },
            'owner': {
                'permissions': Permissions,
                'link': Link,
                'unlink': Unlink,
                'disable': Disable,
                'enable': Enable,
                'gamble': Gamble
            }
        }

    @app_commands.command(name='operator', description='Bot administation (usable by server owners only)')
    @app_commands.describe(
        command='Command to run.',
        extra='Extra information a command may need (optional).'
    )
    async def operator(self, interaction: discord.Interaction, command: str, extra: Optional[str]):
        owner = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Owner')
        operator = interaction.user.id == self.bot.operator
        if not owner:
            await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
            return
        elif not operator:
            await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
            return
        command = command.strip().lower()
        if extra is not None:
            extra = extra.strip()
        if operator and command in self.Commands['operator']:
            await self.Commands['operator'][command](self, user=interaction.user, guild=interaction.guild, interaction=interaction, extra=extra)
        elif command in self.Commands['owner']:
            await self.Commands['owner'][command](self, user=interaction.user, guild=interaction.guild, interaction=interaction, extra=extra)
        else:
            await interaction.response.send_message(f'Command not found. Check the documentation at {self.GetDocLink()} and try again.', ephemeral=True)

    @commands.Cog.listener()
    async def on_ready(self):
        data = self.bot.ReadGlobalData("operator")
        if data:
            if "playing" in data:
                log.debug(f"we are now playing {data['playing']}")
                playing = discord.Game(data["playing"])
                await self.bot.change_presence(status=None, activity=playing)
        log.info(f"{self.qualified_name} loaded and ostensibly ready")

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        if isinstance(guild, discord.Guild):
            commands = await self.bot.tree.sync(guild=guild)
            log.info(
                f"Guild {guild.id} has been joined, syncing {len(commands)} commands."
            )
        elif isinstance(guild, int):
            guild_obj = self.bot.get_guild(guild)
            commands = await self.bot.tree.sync(guild=guild_obj)
            log.info(
                f"Guild {guild_obj.id} has been joined, syncing {len(commands)} commands."
            )

async def setup(bot):
    await bot.add_cog(Operator(bot))