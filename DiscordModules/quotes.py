import discord
from discord.ext import commands
from discord import app_commands
from typing import Optional
import re
import random
import mimetypes
import logging

from DiscordUtil.ChatModule import ChatModule

log = logging.getLogger('WaffleBot.quotes')

random.seed()
never_mention = discord.AllowedMentions(everyone = False, users = False, roles = False, replied_user = False)

class Quotes(ChatModule):
	def GetNextQuoteID(self, channel):
		quote_list = self.GetDataList(channel)
		i = 1
		while True:
			if i in quote_list:
				i += 1
			else:
				return i

	def GetRandomQuote(self, channel):
		quote_list = self.GetDataList(channel)
		numquotes = len(quote_list)
		if numquotes > 0:
			r = random.choice(quote_list)
			return self.ReadData(channel, str(r))

	def GetSpecificQuote(self, channel, id: int):
		quote_list = self.GetDataList(channel)
		path = self.GetDataPath(channel)
		filename = str(id) + '.yaml'
		file = path / filename
		if file.exists():
			return self.ReadData(channel, str(id))

	def Delete(self, guild, quote_id):
		data = self.ReadData(guild, quote_id)
		if data != None:
			if 'attachment' in data:
				self.AttachmentHandler.UnregisterAttachment('Quotes', data['attachment'])
			return self.DeleteData(guild, quote_id)

	@app_commands.command(name='addquote', description='Add a quote. A quote can be text, an attachment, or both (but not neither).')
	@app_commands.describe(
		quote='Text of the quote - Use a semicolon (;) to add new lines. (optional if an attachment exists)',
		attachment='Attachment to embed with the quote (optional if text exists)'
	)
	async def addquote(self, interaction: discord.Interaction, quote: Optional[str], attachment: Optional[discord.Attachment]):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		next_id = self.GetNextQuoteID(interaction.guild_id)
		quote_dict = {
			'author': interaction.user.id,
			'index': next_id
		}
		valid = False
		if quote != None and len(quote) > 0:
			valid = True
			quote = re.sub(r'\s?;\s?', '\n', quote)
			quote_dict['content'] = quote
		if attachment != None:
			valid = True
			filename = await self.SaveAttachment('Quotes', attachment)
			quote_dict['attachment'] = filename
		if valid:
			self.WriteData(interaction.guild_id, next_id, quote_dict)
			await interaction.response.send_message('Quote #' + str(next_id) + ' has been added.')
			self.AttachmentHandler.RemoveUnusedAttachments()
		else:
			await interaction.response.send_message('A quote requires either content, an attachment, or both. Nothing has been done.', ephemeral=True)

	@app_commands.command(name='unquote', description='Delete a quote. Requires the quote ID.')
	@app_commands.describe(
		quote_id='id of the quote to delete',
	)
	async def unquote(self, interaction: discord.Interaction, quote_id: int):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		quote_list = self.GetDataList(interaction.guild_id)
		if quote_id in quote_list:
			if self.Delete(interaction.guild_id, quote_id):
				await interaction.response.send_message('Quote #' + str(quote_id) + ' has been deleted.')
			else:
				await interaction.response.send_message('Quote #' + str(quote_id) + ' could not be deleted (please contact the bot operator).', ephemeral=True)
		else:
			await interaction.response.send_message('Quote #' + str(quote_id) + ' could not be found.', ephemeral=True)

	@app_commands.command(name='quote', description='Get a random (or not-so-random) quote!')
	@app_commands.describe(
		quote_id='(Optional) Specific quote number'
	)
	async def quote(self, interaction: discord.Interaction, quote_id: Optional[int]):
		quote = None
		if quote_id == None:
			quote = self.GetRandomQuote(interaction.guild_id)
		else:
			quote = self.GetSpecificQuote(interaction.guild_id, quote_id)
		if quote != None:
			author_name = 'Unknown User'
			if isinstance(quote['author'], str):
				author_name = quote['author'] + ' (Twitch)'
			else:
				try:
					user = await self.bot.fetch_user(quote['author'])
					author_name = user.global_name
				except:
					pass
			content = f'`Quote #{quote["index"]} by {author_name}`'
			if 'content' in quote:
				content = content + '\n' + quote['content']
			if 'attachment' in quote:
				attach_path = self.GetAttachmentPath(quote['attachment'])
				if attach_path.exists():
					mimetype, irrel = mimetypes.guess_type(attach_path)
					file = discord.File(attach_path, filename = quote['attachment'])
					if mimetype.startswith('image'): # We can do some special sauce to not include file info, only the embed itself
						embed = discord.Embed()
						embed.set_image(url='attachment://' + quote['attachment'])
						await interaction.response.send_message(content, file=file, embed=embed, allowed_mentions=never_mention)
					else:
						await interaction.response.send_message(content, file=file, allowed_mentions=never_mention)
			else:
				await interaction.response.send_message(content, allowed_mentions=never_mention)
		else:
			if quote_id != None:
				await interaction.response.send_message('Could not find a quote with that ID.', ephemeral=True)
			else:
				await interaction.response.send_message('Could not find any quotes.', ephemeral=True)

	@commands.Cog.listener()
	async def on_ready(self):
		log.info(f'{self.qualified_name} loaded and ostensibly ready')

async def setup(bot):
	await bot.add_cog(Quotes(bot))