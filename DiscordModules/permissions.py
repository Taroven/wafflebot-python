import discord
import re
import logging
from typing import Any, List, Union, Optional
from discord.ext import commands
from discord.ui import MentionableSelect
from discord import app_commands, ui, Interaction

from DiscordUtil.ChatModule import ChatModule

log = logging.getLogger('WaffleBot.permissions')

class PermissionsSelect(MentionableSelect):
	async def callback(self, interaction: discord.Interaction):
		permissionData = self.view.perms.ReadPermissions(interaction.guild)
		selected_users = [
			user.id
			for user in self.values
			if isinstance(user, (discord.Member, discord.User))
		]
		selected_roles = [
			role.id
			for role in self.values
			if isinstance(role, discord.Role)
		]

		affected_users = []
		for user_id in selected_users:
			if user_id != interaction.guild.owner_id:
				member = await interaction.guild.fetch_member(user_id)
				affected_users.append(member.mention)
				permissionData['users'][str(user_id)] = self.level
				self.bot.Cache.SetUserLevel(interaction.guild_id, user_id, self.level)

		affected_roles = []
		roleList = {}

		for role in interaction.guild.roles:
			roleList[role.id] = role.mention
		for role_id in selected_roles:
			affected_roles.append(roleList[role_id])
			permissionData['roles'][str(role_id)] = self.level
			self.bot.Cache.SetRoleLevel(interaction.guild_id, role_id, self.level)
		
		if len(affected_users) > 0:
			self.perms.WritePermissions(self.view.message.guild, permissionData)
			user = affected_users[0]
			await self.message.edit(content = f'Set user {user} to {self.bot.Cache.UserLevelNames[self.level]}.', view = None)
		elif len(affected_roles) > 0:
			self.perms.WritePermissions(self.view.message.guild, permissionData)
			role = affected_roles[0]
			await self.message.edit(content = f'Set role {role} to {self.bot.Cache.UserLevelNames[self.level]}.', view = None)
		else:
			await self.message.edit(content = 'Something went wrong. Did you try to pick yourself?', view = None)


class PermissionsView(discord.ui.View):
	def __init__(self, perms, level):
		super().__init__()
		self.level = level
		self.perms = perms
		self.bot = perms.bot
		perm = PermissionsSelect(placeholder='Select a user or role.')
		perm.level = self.level
		perm.perms = self.perms
		perm.bot = self.bot
		self.select = perm
		self.add_item( perm )

	def ReadPermissions(self, guild):
		self.bot.Cache.AddChannel(guild.id, guild.owner_id)
		permissionData = self.perms.ReadData(guild, 'permissions')
		if permissionData == None:
			permissionData = {
				'users': {},
				'roles': {}
			}
			self.perms.WritePermissions(guild, permissionData)
		for role_id, role_perm in permissionData['roles'].items():
			self.bot.Cache.SetRoleLevel(guild.id, role_id, role_perm)
		for user_id, user_perm in permissionData['users'].items():
			self.bot.Cache.SetUserLevel(guild.id, user_id, user_perm)
		return permissionData

	def WritePermissions(self, guild, data):
		if 'users' not in data:
			return
		if 'roles' not in data:
			return
		self.bot.Cache.AddChannel(guild.id, guild.owner_id)
		self.perms.WriteData(guild, 'permissions', data)

class Permissions(ChatModule):
	def ReadPermissions(self, guild):
		permissions = self.bot.Cache.AddChannel(guild.id, guild.owner_id)
		savedData = self.ReadData(guild, 'permissions')
		if savedData == None:
			savedData = self.WritePermissions(guild, permissions)
		for role_id, role_perm in savedData['roles'].items():
			self.bot.Cache.SetRoleLevel(guild.id, role_id, role_perm)
		for user_id, user_perm in savedData['users'].items():
			self.bot.Cache.SetUserLevel(guild.id, user_id, user_perm)
		return self.bot.Cache.AddChannel(guild.id, guild.owner_id)

	def WritePermissions(self, guild, data):
		if 'users' not in data:
			return
		if 'roles' not in data:
			return
		self.WriteData(guild, 'permissions', data)
		return self.bot.Cache.AddChannel(guild.id, guild.owner_id)

	# Events to reload permissions
	@commands.Cog.listener()
	async def on_ready(self):
		log.info(f'{self.qualified_name} loaded and ostensibly ready')
		for guild in self.bot.guilds:
			log.debug(f'on_ready - guild: {guild.id} owner: {guild.owner_id}')
			self.ReadPermissions(guild)

	@commands.Cog.listener()
	async def on_guild_join(self, guild):
		self.ReadPermissions(guild)

	@commands.Cog.listener()
	async def on_guild_available(self, guild):
		self.ReadPermissions(guild)

	# Actual command
	@app_commands.command(name='setpermissions', description="Set a user/role's permission level.")
	@app_commands.describe(
		level='Level to set users/roles to. Lower is higher privileges (1 for Admin, 2 for Op, etc)'
	)
	async def setpermissions(self, interaction: discord.Interaction, level: app_commands.Range[int,1,5]):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Owner')
		if not permitted:
			await interaction.response.send_message('Only the server owner may use this command.', ephemeral = True)
			return
		view = PermissionsView(self, level)
		await interaction.response.send_message(f'Select user or role to set to level {self.bot.Cache.UserLevelNames[level]}:', view = view, ephemeral = True)
		view.message = await interaction.original_response()
		view.select.message = view.message

async def setup(bot):
	await bot.add_cog(Permissions(bot))