import discord
from discord.ext import commands
from discord import app_commands
from typing import Optional
from pathlib import Path
import re
import logging

from DiscordUtil.ChatModule import ChatModule
from Util.RandomHandler import RandomHandler

log = logging.getLogger('WaffleBot.random')

class Random(ChatModule):
	def ListChoices(self, guild_id):
		path = self.GetDataPath(guild_id)
		_list = list(path.glob('*.yaml'))
		result = "Current choice lists:"
		for i in _list:
			result += f'\n- {i.stem}'
		return result

	@app_commands.command(name='math', description='Do math!')
	@app_commands.describe(
		formula='Math to do (or any algebraic equation)'
	)
	async def math(self, interaction: discord.Interaction, formula: str):
		try:
			result = numexpr.evaluate(formula.strip())
			await interaction.response.send_message(str(result))
		except:
			await interaction.response.send_message('Invalid syntax. Read https://numexpr.readthedocs.io/en/latest/user_guide.html#supported-operators for documentation.', ephemeral=True)

	@app_commands.command(name='dice', description='Roll dice!')
	@app_commands.describe(
		dice='Dice string to roll (ie: 1d6, 3d20+4)'
	)
	async def dice(self, interaction: discord.Interaction, dice: str):
		try:
			r = RandomHandler(dice)
			dstring = r.DiceString()
			roll = r.Dice()
			await interaction.response.send_message(f'`Rolling {dstring}:` {roll}')
		except:
			await interaction.response.send_message(f'`{dice}` could not be parsed.', ephemeral=True)

	@app_commands.command(name='eightball', description='Ask the magic 8-Ball!')
	@app_commands.describe(
		question='Question to ask'
	)
	async def eightball(self, interaction: discord.Interaction, question: str):
		r = RandomHandler(question)
		result = r.EightBall()
		await interaction.response.send_message(f"||{question}||\n\n*{result}*")

	@app_commands.command(name='pick', description='Pick choices from a predefined list')
	@app_commands.describe(
		name='List of choices to pick from (invalid or missing will reply with valid options)',
		iterations='Number of items to pick (default 1, accepts dice strings)',
		nodupes='Set to anything other than empty to disallow duplicate choices'
	)
	async def pick(self, interaction: discord.Interaction, name: str, iterations: Optional[str], nodupes: Optional[str]):
		pop = False
		iter = 1
		if isinstance(nodupes, str | int):
			pop = True
		if iterations is None:
			iter = 1
		elif isinstance(iterations,str):
			if iterations.isdigit():
				iter = int(iterations)
			else:
				try:	
					iter = int(RandomHandler(iterations).Dice())
				except Exception as e:
					iter = 1

		word = name.lower().strip()
		if not word.isalnum():
			await interaction.response.send_message(f'`{word}` cannot be used as a name.', ephemeral=True)
			return
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await interaction.response.send_message(self.ListChoices(interaction.guild_id), ephemeral=True)
			return
		
		data = self.ReadData(interaction.guild_id, word)
		if data is None:
			await interaction.response.send_message(self.ListChoices(interaction.guild_id), ephemeral=True)
			return

		picks = RandomHandler(data).Get(iterations=iter, pop=pop)
		result = f'`Picks from {word}:`'
		if iter > 1:
			result = f'`Picks from {word} with {iter} ({iterations}) iterations:`'
		for i in picks:
			result += f'\n- {i}'
		await interaction.response.send_message(result)
	
	@app_commands.command(name='addchoices', description='Add a list for use in /pick')
	@app_commands.describe(
		name='Name of the new list',
		choices='List of choices - Use semicolon (;) to separate'
	)
	async def addchoices(self, interaction: discord.Interaction, name: str, choices: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		word = name.lower().strip()
		if not word.isalnum():
			await interaction.response.send_message(f'`{word}` cannot be used as a name.', ephemeral=True)
			return
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await interaction.response.send_message(f'`{word}` cannot be used as a name.', ephemeral=True)
			return
		choices = re.sub(r'\s?;\s?',';',choices) # clean up extraneous spaces
		data = choices.split(';')
		self.WriteData(interaction.guild_id, word, data)
		result = f'List `{word}` has been added with the following choices:'
		for i in data:
			result += f'\n- {i}'
		await interaction.response.send_message(result, ephemeral=True)

	@app_commands.command(name='unchoice', description='Remove a list of /pick choices')
	@app_commands.describe(
		name='Name of the list to delete'
	)
	async def unchoice(self, interaction: discord.Interaction, name: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Trusted')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		word = word.lower().strip()
		try:
			self.ValidFileName(self.GetYamlName(word))
		except ValueError:
			await interaction.response.send_message(f'`{word}` cannot be used as a name.', ephemeral=True)
			return
		self.DeleteData(interaction.guild_id, word)
		await interaction.response.send_message(f'Choice list `{word}` has been deleted.', ephemeral=True)

async def setup(bot):
	await bot.add_cog(Random(bot))