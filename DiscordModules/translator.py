import discord
from discord.ext import commands, tasks
from discord import app_commands
import deep_translator
from deep_translator import GoogleTranslator
from typing import Optional
from time import sleep
import re
import logging
import asyncio
import random

from DiscordUtil.ChatModule import ChatModule
log = logging.getLogger('WaffleBot.translator')
never_mention = discord.AllowedMentions(everyone = False, users = False, roles = False, replied_user = False)

random.seed()

allLanguages = GoogleTranslator().get_supported_languages(as_dict=True)
languages = {
	'afrikaans': 'af',
	'arabic': 'ar',
	'armenian': 'hy',
	'basque': 'eu',
	'belarusian': 'be',
	'bulgarian': 'bg',
	'chinese (simplified)': 'zh-CN',
	'chinese (traditional)': 'zh-TW',
	'dutch': 'nl',
	'english': 'en',
	'esperanto': 'eo',
	'finnish': 'fi',
	'french': 'fr',
	'german': 'de',
	'greek': 'el',
	'gujarati': 'gu',
	'hindi': 'hi',
	'indonesian': 'id',
	'irish': 'ga',
	'italian': 'it',
	'japanese': 'ja',
	'kannada': 'kn',
	'korean': 'ko',
	'maltese': 'mt',
	'marathi': 'mr',
	'portuguese': 'pt',
	'pashto': 'ps',
	'persian': 'fa',
	'polish': 'pl',
	'punjabi': 'pa',
	'russian': 'ru',
	'sesotho': 'st',
	'sinhala': 'si',
	'spanish': 'es',
	'sundanese': 'su',
	'swedish': 'sv',
	'tamil': 'ta',
	'telugu': 'te',
	'thai': 'th',
	'turkish': 'tr',
	'ukrainian': 'uk',
	'vietnamese': 'vi'
}

def TranslateString(args):
	while len(args['queue']) > 3:
		sleep(1)
	args['queue'].append(args['ref'])
	args['result'] = None
	args['exception'] = None
	try:
		args['result'] = GoogleTranslator(source=args['source'], target=args['target']).translate(args['origin'])
	except Exception as e:
		args['exception'] = e
	finally:
		args['queue'].remove(args['ref'])

def ReadException(e):
	if isinstance(e, deep_translator.exceptions.NotValidPayload):
		return 'Input is invalid. At least some portion needs to be normal text.'
	elif isinstance (e, deep_translator.exceptions.TooManyRequests):
		return 'Waffles hit the API limit for the translation service. Try again tomorrow.'
	elif isinstance(e, deep_translator.exceptions.NotValidLength):
		return 'Input is too short or too long.'
	elif isinstance(e, deep_translator.exceptions.TranslationNotFound):
		return 'No translation was found. Try a different input or language.'
	elif isinstance(e, deep_translator.exceptions.RequestError):
		return 'Something went wrong when connecting to the API. Try again.'
	elif isinstance(e, deep_translator.exceptions.ServerException):
		return 'The server returned an API error. Try again.'

class Translator(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.transQueue = []

	def Progress(self, text, **kwargs):
		progress = '\n`'
		if 'progress' in kwargs:
			if 'total' in kwargs:
				current = len(kwargs['progress']) - 1
				percent = round(current / kwargs['total'] * 100)
				progress += f'{percent}'
		progress += '% complete`\n'
		warn = 'This will take some time.'
		if 'total' in kwargs:
			if kwargs['total'] > 24:
				warn = 'This will take an extremely long time.'

		return f'`Enshittifying. {warn}`{progress}>>> {text}'

	async def GetLanguage(self, target: Optional[str], **kwargs):
		if target:
			target = target.strip().lower()
		if target == 'help':
			header = 'Supported languages: ```\n'
			body = ''
			footer = '```'
			for k,v in allLanguages.items():
				body += f'{k} ({v})\n'
			if 'interaction' in kwargs:
				await kwargs['interaction'].response.send_message(content = header + body + footer, ephemeral = True)
			elif 'message' in kwargs:
				await message.reply(content = header + body + footer)
		elif target in allLanguages.keys():
			return allLanguages[target]
		elif target in allLanguages.values():
			t = [i for i in allLanguages if allLanguages[i]==target]
			if len(t) > 0:
				return t[0]
		else:
			return 'en'

	async def Enshittify(self, origin: str, target: str = 'en', **kwargs):
		halt = False
		exception = None
		languageList = [target]
		times = kwargs['times']
		log.info(f'enshittifying with {times} iterations')

		transArgs = {
			'queue': self.transQueue,
			'ref': kwargs['interaction'],
			'exception': None,
			'result': origin # little hack to get this loop going
		}
		for x in range(times):
			transTarget = languageList[-1]
			while transTarget == languageList[-1]: # ensure a different language each time
				transTarget = allLanguages[random.choice(list(allLanguages))]
			transArgs['source'] = 'auto'
			transArgs['target'] = transTarget
			transArgs['origin'] = transArgs['result']
			await asyncio.gather( asyncio.to_thread(TranslateString, transArgs) )
			if transArgs['exception'] != None:
				halt = True
				break
			else:
				languageList.append(transTarget)
				await kwargs['interaction'].edit_original_response(content = self.Progress(origin, progress = languageList, total = times))
		if transArgs['exception'] != None:
			await kwargs['interaction'].edit_original_response(content = ReadException(transArgs['exception']))
			log.warn(str(transArgs['exception']))
			return
		else:
			transArgs['source'] = 'auto'
			transArgs['target'] = target
			transArgs['origin'] = transArgs['result']
			await asyncio.gather( asyncio.to_thread(TranslateString, transArgs) )
			if transArgs['exception'] != None:
				await kwargs['interaction'].edit_original_response(content = ReadException(transArgs['exception']))
				return
			header = '||`'
			header += '>'.join(languageList)
			header += '`||\n'
			content = header + ">>> " + transArgs['result']
			if 'message' in kwargs:
				await kwargs['message'].edit(content = content)
			elif 'interaction' in kwargs:
				await kwargs['interaction'].edit_original_response(content = content)

	async def Translate(self, source: str, target: str, origin: str, **kwargs):
		transArgs = {
			'exception': None,
			'queue': self.transQueue,
			'source': source,
			'target': target,
			'origin': origin,
			'queue': self.transQueue,
		}

		# provide a unique reference for the queue (not used for anything else)
		if 'message' in kwargs:
			transArgs['ref'] = kwargs['message']
		elif 'interaction' in kwargs:
			transArgs['ref'] = kwargs['interaction']

		await asyncio.gather( asyncio.to_thread(TranslateString, transArgs) )
		if transArgs['exception'] != None:
			if 'message' in kwargs:
				await kwargs['message'].edit(content = ReadException(transArgs['exception']))
			elif 'interaction' in kwargs:
				await kwargs['interaction'].edit_original_response(content = ReadException(transArgs['exception']))
		else:
			if 'message' in kwargs:
				await kwargs['message'].edit(content = transArgs['result'])
			elif 'interaction' in kwargs:
				await kwargs['interaction'].edit_original_response(content = transArgs['result'])

	@app_commands.command(name='translate', description="Translate something to a desired language.")
	@app_commands.describe(
		language='What language to translate to (optional, defaults to English - pass "help" to get a list)',
		text='Text to translate.'
	)
	async def translate(self, interaction: discord.Interaction, text: str, language: Optional[str]):
		if language:
			language = language.strip().lower()
		target = await self.GetLanguage(language, interaction = interaction)
		if target:
			await interaction.response.send_message(content = 'This may take a moment.', ephemeral = False)
			await asyncio.create_task(self.Translate('auto', target, text, interaction = interaction))

	@app_commands.command(name='enshittify', description="Translate something to... something.")
	@app_commands.describe(
		language='What language to enshittify to (optional, defaults to English - pass "help" to get a list)',
		iterations='How enshittified should this be? (optional, 4-100, defaults to 12)',
		text='Text to enshittify.'
	)
	async def enshittify(self, interaction: discord.Interaction, text: str, iterations: Optional[int], language: Optional[str]):
		if self.IsCommandDisabled('enshittify', interaction.guild):
			await interaction.response.send_message(content = 'This command is disabled by your server owner.', ephemeral = True)
			return
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Voice')
		if not permitted:
			await interaction.response.send_message('Because this command uses a lot of remote processing, not just anyone can use it.', ephemeral = True)
			return
		if language:
			language = language.strip().lower()
		target = await self.GetLanguage(language, interaction = interaction)
		if target:
			if iterations:
				iterations = max(4, min(iterations, 100))
			else:
				iterations = 12

			await interaction.response.send_message(content = self.Progress(text, progress = [target], total = iterations), ephemeral = False)
			await asyncio.create_task(self.Enshittify(text, target, interaction = interaction, times = iterations))

async def setup(bot):
	await bot.add_cog(Translator(bot))
