import discord
from discord.ext import commands
from discord import app_commands
from typing import Optional
import re
import logging
from DiscordUtil.ChatModule import ChatModule

log = logging.getLogger('WaffleBot.timers')

class Timers(ChatModule):
	def PrintTimer(self, data):
		content = f'`Timer {data["name"]}`:\n```\n'
		content += f'Every {data["interval"]} minutes with {data["active"]} active chatters.\n\n'
		content += f'Content: {data["content"]}'
		content += '```'
		return content

	@app_commands.command(name='addtimer', description='Add a timer to Twitch chat (Admin only)')
	@app_commands.describe(
		name='Name of the timer (used for identification only)',
		content='Text to send when the timer triggers',
		interval='Number of minutes between triggers (default: 15)',
		active='Number of active chatters required to trigger (default: 2)',
	)
	async def addtimer(self, interaction: discord.Interaction, name: str, content: str, interval: Optional[int] = 15, active: Optional[int] = 2):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('Only the server owner or administrators may use this command.', ephemeral = True)
			return
		name = name.strip().lower()
		try:
			fname = self.GetYamlName(name)
			self.ValidFileName(fname)
		except:
			await interaction.response.send_message('The name for this timer is invalid. Please choose something else.', ephemeral = True)
			return
		if len(name) > 32 or not name.isalnum():
			await interaction.response.send_message('The specified timer name is invalid. Timer names must be 32 characters or less and alphanumeric.', ephemeral = True)
			return
		data = self.ReadData(interaction.guild_id, 'timers')
		timer = {
			'name': name,
			'content': content,
			'interval': interval,
			'active': active
		}
		self.WriteData(interaction.guild_id, name, timer)
		self.TouchData(interaction.guild_id, '.modified')
		await interaction.response.send_message(self.PrintTimer(timer), ephemeral = True)


	@app_commands.command(name='untimer', description='Remove a timer from Twitch chat (Admin only)')
	@app_commands.describe(
		name='Name of the timer to delete (use /timers to list)',
	)
	async def untimer(self, interaction: discord.Interaction, name: str):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('Only the server owner or administrators may use this command.', ephemeral = True)
			return
		name = name.strip().lower()
		try:
			fname = self.GetYamlName(name)
			self.ValidFileName(fname)
		except:
			await interaction.response.send_message('The name for this timer is invalid.', ephemeral = True)
			return
		if len(name) > 32 or not name.isalnum():
			await interaction.response.send_message('The specified timer name is invalid. Timer names must be 32 characters or less and alphanumeric.', ephemeral = True)
			return
		self.DeleteData(interaction.guild_id, name)
		self.TouchData(interaction.guild_id, '.modified')
		await interaction.response.send_message(f'Timer {name} has been deleted.', ephemeral = True)

	@app_commands.command(name='timers', description='Get a list of Twitch chat timers (Admin only)')
	@app_commands.describe(
		name='(Optional) Timer to get specific information about',
	)
	async def timers(self, interaction: discord.Interaction, name: Optional[str]):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('Only the server owner or administrators may use this command.', ephemeral = True)
			return

		path = self.GetDataPath(interaction.guild_id)
		timer_list = list(path.glob('*.yaml'))
		if len(timer_list) > 0:
			content = 'List of timers:'
			for timer in timer_list:
				content += f'\n- {timer}'
			await interaction.response.send_message(content, ephemeral = True)
		else:
			await interaction.response.send_message('No timers found.', ephemeral = True)

async def setup(bot):
	await bot.add_cog(Timers(bot))