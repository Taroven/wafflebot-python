import discord
from discord.ext import commands
from discord import app_commands
from typing import Optional
import re
import time
import asyncio
import random
from datetime import timedelta

from DiscordUtil.ChatModule import ChatModule
from Util.Gamble import Gamble
from Util.UserCache import UserCache
from Util.DuelHandler import DuelHandler

initial_points = 10
points_per_interval = 3
time_interval = 21600

class GambleResults():
	def __init__(self, data):
		self.data = data

	def Get(self):
		results = getattr(self, self.data['game'])
		return results()

	def roulette(self):
		if self.data['win']:
			payout = self.data['mult'] * self.data['bid']
			result = f'You won **{payout}** points'
			if self.data['mult'] > 1:
				result += f" with a **{self.data['mult']}x** multiplier"
			result += '!'
			return result
		else:
			return 'Sorry, you lost. Better luck next time.'

	def slots(self):
		result = "You spun: "
		for i in self.data['slots']:
			result += i + ' '
		if self.data['win']:
			payout = self.data['mult'] * self.data['bid']
			result += f"\n\nYou won **{payout}** points with a **{self.data['mult']}x** multiplier!"
			return result
		else:
			result += "\n\nBetter luck next time."
			return result

class GambleSelect(discord.ui.Select):
	def __init__(self, cog, **kwargs):
		self.cog = cog
		self.gamble_options = kwargs
		selections = []
		for i in cog.games_titles:
			selections.append( discord.SelectOption(label=i) )
		super().__init__(placeholder="Pick your poison", options=selections)

	async def callback(self, interaction: discord.Interaction):
		data = self.cog.ReadData(interaction.guild_id, interaction.user.id)
		if data is None: # this should never happen, but if it does we should let the user know.
			await interaction.response.edit_message(view=None, content="Something went wrong. Please try again later.")
			return
		if data['points'] < self.gamble_options['bid']: # prevent multiple concurrent gambles over balance
			await interaction.response.edit_message(view=None, content="Aborted - You don't have enough points remaining.")
			return
		self.gamble_options['game'] = self.values[0]
		gamble = Gamble(**self.gamble_options)
		result = gamble.Play()
		result_output = GambleResults(result).Get()
		if result['win']:
			data['points'] += result['mult'] * self.gamble_options['bid']
		else:
			data['points'] -= self.gamble_options['bid']
		self.cog.WriteData(interaction.guild_id, interaction.user.id, data)
		await interaction.response.edit_message(view=None, content=result_output)

class GambleView(discord.ui.View):
	def __init__(self, cog, **kwargs):
		super().__init__(timeout=180)
		self.add_item(GambleSelect(cog, **kwargs))

class RaffleButton(discord.ui.Button):
	def __init__(self, cog, guild_id, t):
		self.cog = cog
		self.t = t
		super().__init__(label="Enter!", emoji="🤩", style=discord.ButtonStyle.primary)

	async def callback(self, interaction: discord.Interaction):
		data = self.cog.Raffles[interaction.guild_id][self.t]
		if time.time() > data['end_time']:
			return
		if interaction.user.id not in data['users']:
			data['users'][interaction.user.id] = interaction.user
			await self.cog.BuildRaffle(interaction, self.t)
			try:
				await interaction.response.defer()
			except:
				pass

class RaffleView(discord.ui.View):
	def __init__(self, cog, guild_id, t):
		super().__init__(timeout=None)
		self.add_item(RaffleButton(cog, guild_id, t))

class DuelButton(discord.ui.Button):
	def __init__(self, label, emoji):
		super().__init__(label=label, emoji=emoji, style=discord.ButtonStyle.primary)

class DuelAcceptButton(DuelButton):
	def __init__(self, cog, data):
		self.cog = cog
		self.data = data
		super().__init__('I accept!', '⚔️')

	async def callback(self, interaction: discord.Interaction):
		if interaction.user.id == self.data['user_b'].id:
			data_a = self.cog.AddPoints(interaction.guild_id, interaction.user.id)
			data_b = self.cog.AddPoints(interaction.guild_id, self.data['user_b'].id)
			if data_a['points'] >= self.data['bid'] and data_b['points'] >= self.data['bid']:
				# do the duel
				duel_result = DuelHandler(self.data['user_a'].display_name, self.data['user_b'].display_name, False).Duel()
				# tabulate
				winner = self.data['user_a'] if self.data['user_a'].display_name == duel_result['winner'] else self.data['user_b']
				loser = self.data['user_b'] if self.data['user_a'].display_name == duel_result['winner'] else self.data['user_a']
				data_winner = self.cog.AddPoints(interaction.guild_id, winner.id)
				data_loser = self.cog.AddPoints(interaction.guild_id, loser.id)
				data_winner['points'] += self.data['bid']
				data_loser['points'] -= self.data['bid']
				self.cog.WriteData(interaction.guild_id, winner.id, data_winner)
				self.cog.WriteData(interaction.guild_id, loser.id, data_loser)
				await interaction.response.edit_message(content=duel_result['battle'] + f"\n\nPoints won: **{self.data['bid']}**", view=None)
			else:
				await interaction.response.edit_message(content="The duel has been canceled. One of the participants no longer has enough points.", view=None)
		try:
			await interaction.response.defer()
		except:
			pass

class DuelRejectButton(DuelButton):
	def __init__(self, cog, data):
		self.cog = cog
		self.data = data
		super().__init__('I reject!', '🐔')

	async def callback(self, interaction: discord.Interaction):
		if interaction.user.id == self.data['user_b'].id:
			await interaction.response.edit_message(content=f"The duel between {self.data['user_a'].display_name} and {self.data['user_b'].display_name} has been declined.", view=None)
		try:
			await interaction.response.defer()
		except:
			pass

class DuelView(discord.ui.View):
	def __init__(self, cog, data):
		super().__init__(timeout=600)
		self.add_item(DuelAcceptButton(cog, data))
		self.add_item(DuelRejectButton(cog, data))


class Gambling(ChatModule):
	def __init__(self, bot):
		super().__init__(bot)
		self.games = Gamble().GetGames()
		self.games_titles = [word.capitalize() for word in self.games]
		self.Cache = UserCache()
		self.Raffles = {}

	def AddPoints(self, guild, user):
		data = self.ReadData(guild, user)
		if data is None:
			data = {'points': initial_points, 'last': time.time()}
			self.WriteData(guild, user, data)
			return data
		ct = time.time()
		time_since_last = ct - data['last']
		if time_since_last > time_interval: # 22 hours since last update
			data['points'] += points_per_interval
			data['last'] = ct
			self.WriteData(guild, user, data)
		return data

	async def RaffleTask(self, guild_id, t):
		data = self.Raffles[guild_id][t]
		while time.time() < data['end_time']:
			await asyncio.sleep(1)
		if len(data['users']) > 0:
			r = random.SystemRandom()
			winner = r.choice(list(data['users'].keys()))
			winner_mention = data['users'][winner].mention
			winner_data = self.AddPoints(guild_id, winner)
			winner_data['points'] += data['points']
			self.WriteData(guild_id, winner, winner_data)
			content = f"{winner_mention} won the raffle for **{data['points']}** points!"
			await data['message'].edit(content=content, view=None)
		else:
			await data['message'].edit(content = f"Nobody entered the raffle.", view=None)

	async def BuildRaffle(self, interaction, t):
		data = self.Raffles[interaction.guild_id][t]
		base_content = f"A raffle is in progress for **{data['points']}** points, lasting **{data['minutes']}** minutes ({data['timestamp']})"
		if 'built' not in data:
			await data['message'].edit(content=base_content, view=RaffleView(self, interaction.guild_id, t))
			data['built'] = True
		else:
			num_users = len(data['users'])
			extra_content = f"\n\nNumber of entries: **{num_users}**"
			await data['message'].edit(content=base_content + extra_content)

	async def DoGambling(self, interaction, game, bid):
		data = self.AddPoints(interaction.guild_id, interaction.user.id)
		if bid < 0:
			await interaction.response.send_message('Bid must be a positive value.', ephemeral=True)
		if bid > data['points']:
			next_time = int(data['last']) + time_interval
			timestamp = f"<t:{next_time}:R>"
			await interaction.response.send_message(f"You have {data['points']} points remaining. You will receive {points_per_interval} points every {int(time_interval / 60 / 60)} hours ({timestamp}).", ephemeral=True)
			return
		gamble = Gamble(game=game, bid=bid)
		result = gamble.Play()
		result_output = GambleResults(result).Get()
		if result['win']:
			data['points'] += result['mult'] * bid
		else:
			data['points'] -= bid
		self.WriteData(interaction.guild_id, interaction.user.id, data)
		ephemeral_data = self.bot.ReadGlobalData('gamble')
		ephemeral = True
		if ephemeral_data is not None and interaction.guild_id in ephemeral_data:
			ephemeral = True if bid == 0 else ephemeral_data[interaction.guild_id]
		await interaction.response.send_message(result_output, ephemeral=ephemeral)

	# Give users points per day (using a minimal cache to avoid excessive IO)
	@commands.Cog.listener()
	async def on_message(self, message):
		if message.author.id == self.bot.user.id:
			return
		user = message.author.id
		guild = message.guild.id
		if self.Cache.GetProp(guild, user, 'last') is None:
			data = self.AddPoints(guild, user)
			self.Cache.SetProp(guild, user, 'last', data['last'])
		ct = time.time()
		self.Cache.SetProp(guild, user, 'seen', ct)
		if ct - self.Cache.GetProp(guild, user, 'last') > time_interval:
			data = self.AddPoints(guild, user)
			self.Cache.SetProp(guild, user, 'last', data['last'])

	@app_commands.command(name='slots', description='Take a spin at the slot machine!')
	@app_commands.describe(
		bid='How many points to wager'
	)
	async def slots(self, interaction: discord.Interaction, bid: int):
		if self.IsCommandDisabled('gamble', interaction.guild):
			await interaction.response.send_message(content = 'This command is disabled by your server owner.', ephemeral = True)
			return
		await self.DoGambling(interaction, 'slots', bid)

	@app_commands.command(name='roulette', description='Take a spin at the roulette wheel!')
	@app_commands.describe(
		bid='How many points to wager'
	)
	async def roulette(self, interaction: discord.Interaction, bid: int):
		if self.IsCommandDisabled('gamble', interaction.guild):
			await interaction.response.send_message(content = 'This command is disabled by your server owner.', ephemeral = True)
			return
		await self.DoGambling(interaction, 'roulette', bid)

	@app_commands.command(name='gamble', description='Spend points, score big. Or not.')
	@app_commands.describe(
		bid='How many points to wager'
	)
	async def gamble(self, interaction: discord.Interaction, bid: int):
		if self.IsCommandDisabled('gamble', interaction.guild):
			await interaction.response.send_message(content = 'This command is disabled by your server owner.', ephemeral = True)
			return
		data = self.AddPoints(interaction.guild_id, interaction.user.id)
		next_time = int(data['last']) + time_interval
		timestamp = f"<t:{next_time}:R>"
		if bid > data['points'] and bid >= 0:
			await interaction.response.send_message(f"You only have **{data['points']}** points remaining. You get **{points_per_interval}** points every {int(time_interval / 60 / 60)} hours ({timestamp}).", ephemeral=True)
			return
		else:
			view = GambleView(self, bid=bid)
			await interaction.response.send_message(content=f"You have **{data['points']}** points remaining. You get **{points_per_interval}** points every {int(time_interval / 60 / 60)} hours ({timestamp}). What will you bid **{bid}** points on?", view=view, ephemeral=True)

	@app_commands.command(name='givepoints', description='Give your points away')
	@app_commands.describe(
		points='How many points to give',
		user='Who to give them to'
	)
	async def givepoints(self, interaction: discord.Interaction, user: discord.Member, points: int):
		data_a = self.AddPoints(interaction.guild_id, interaction.user.id)
		data_b = self.AddPoints(interaction.guild_id, user.id)
		if data_a['points'] >= points and points > 0:
			data_a['points'] -= points
			data_b['points'] += points
			self.WriteData(interaction.guild_id, interaction.user.id, data_a)
			self.WriteData(interaction.guild_id, user.id, data_b)
			await interaction.response.send_message(f'You have given {points} points to {user.mention}.')
		else:
			next_time = int(data_a['last']) + time_interval
			timestamp = f"<t:{next_time}:R>"
			await interaction.response.send_message(f"You only have **{data_a['points']}** points remaining. You get **{points_per_interval}** points per day ({timestamp}).", ephemeral=True)

	@app_commands.command(name='addpoints', description='Add points to a user (Admin+)')
	@app_commands.describe(
		points='How many points to add (negative numbers will subtract points)',
		user='Who to give them to'
	)
	async def addpoints(self, interaction: discord.Interaction, user: discord.Member, points: int):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		data = self.AddPoints(interaction.guild_id, user.id)
		data['points'] += points
		data['points'] = max(data['points'], 0)
		self.WriteData(interaction.guild_id, user.id, data)
		if points > 0:
			await interaction.response.send_message(f"**{points}** points have been added to {user.mention}. They now have **{data['points']}**.")
		else:
			await interaction.response.send_message(f"**{points}** points have been removed from {user.mention}. They now have **{data['points']}**.")

	@app_commands.command(name='duel', description='Duel someone for points!')
	@app_commands.describe(
		points='How much do you wager?',
		user='User to duel'
	)
	async def duel(self, interaction: discord.Interaction, user: discord.Member, points: int):
		if interaction.user.id == user.id:
			await interaction.response.send_message('You cannot duel yourself.', ephemeral=True)
			return
		data_a = self.AddPoints(interaction.guild_id, interaction.user.id)
		data_b = self.AddPoints(interaction.guild_id, user.id)
		if data_a['points'] >= points and data_b['points'] >= points:
			data = {'user_a': interaction.user, 'user_b': user, 'bid': points}
			await interaction.response.send_message(content=f"{user.mention}, {interaction.user.mention} is challenging you to a duel for **{points}** points.\n\nDo you accept?", view=DuelView(self,data))
		else:
			result = ""
			if data_a['points'] < points and data_b['points'] < points:
				result += "Neither of you have enough points."
			elif data_a['points'] < points:
				result += "You don't have enough points."
			else:
				result += "Your opponent doesn't have enough points."
			result += f"\n- You have {data_a['points']} points.\n- Your opponent has {data_b['points']} points."
			await interaction.response.send_message(result, ephemeral=True)

	@app_commands.command(name='raffle', description='Start a raffle for points (Admin+)')
	@app_commands.describe(
		points='How many points to raffle'
	)
	async def raffle(self, interaction: discord.Interaction, points: int, minutes: int):
		permitted = await self.IsCommandPermitted(interaction.guild_id, interaction.user.id, 'Admin')
		if not permitted:
			await interaction.response.send_message('You do not have permission to do this.', ephemeral=True)
			return
		if points < 1 or minutes < 1 or minutes > 15:
			await interaction.response.send_message('`points` must be 1 or more, `minutes` must be between 1 and 15.', ephemeral=True)
			return
		ct = time.time()
		timestamp_time=int(ct) + (minutes * 60)
		timestamp = f"<t:{timestamp_time}:R>"
		base_content = f"A raffle is in progress for **{points}** points, lasting **{minutes}** minutes ({timestamp})"
		await interaction.response.send_message(content=base_content)
		message = await interaction.original_response()
		if interaction.guild_id not in self.Raffles:
			self.Raffles[interaction.guild_id] = {}
		self.Raffles[interaction.guild_id][ct] = {'message': message, 'points': points, 'minutes': minutes, 'end_time': timestamp_time, 'timestamp': timestamp, 'users': {}}
		await self.BuildRaffle(interaction, ct)
		asyncio.create_task(self.RaffleTask(interaction.guild_id, ct))

async def setup(bot):
	await bot.add_cog(Gambling(bot))