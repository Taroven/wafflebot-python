import pathlib
from pathlib import Path
import os
import asyncio
import logging
import time

log = logging.getLogger('WaffleBot-Util.FileWatch')
class FileWatcher():
	def __init__(self, obj, path: Path, interval: int, isAsync, callback, **kwargs):
		self.obj = obj
		self.callback = callback
		self.interval = interval
		self.path = path
		self.exists = None
		self.mtime = None
		self.isAsync = isAsync
		if 'extra' in kwargs:
			self.extra = kwargs['extra']
		else:
			self.extra = {}
		asyncio.create_task(self.Loop())

	async def Check(self):
		exists = self.path.is_file()
		mtime = None
		if exists:
			mtime = self.path.stat().st_mtime
		else:
			mtime = None

		if exists != self.exists or mtime != self.mtime:
			log.debug(f'File {self.path} changed')
			self.exists = exists
			self.mtime = mtime
			if self.isAsync:
				await self.callback(self.obj, **self.extra)
			else:
				self.callback(self.obj, **self.extra)

	async def Loop(self):
		while True:
			await self.Check()
			await asyncio.sleep(self.interval)