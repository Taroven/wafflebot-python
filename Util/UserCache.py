
class UserCache():
	def __init__(self):
		self.Channels = {}

	def GetAll(self, channel):
		if channel not in self.Channels:
			self.Channels[channel] = {}
		return self.Channels[channel]

	def Get(self, channel, user, default=None):
		try:
			return self.Channels[channel][user]
		except:
			return default

	def Set(self, channel, user, data):
		if channel not in self.Channels:
			self.Channels[channel] = {}
		self.Channels[channel][user] = data

	def Remove(self, channel, user):
		try:
			self.Channels[channel].pop(user)
		except:
			pass

	def SetProp(self, channel, user, key, val):
		if channel not in self.Channels:
			self.Channels[channel] = {}
		if user not in self.Channels[channel]:
			self.Channels[channel][user] = {}
		self.Channels[channel][user][key] = val

	def GetProp(self, channel, user, key, default=None):
		try:
			return self.Channels[channel][user][key]
		except:
			return default

	def RemoveProp(self, channel, user, key):
		try:
			self.Channels[channel][user].pop(key)
		except:
			pass