import re
from Util.MessageData import MessageData

def display_name(ctx, s):
	return re.sub('%U', ctx.display_name, s)

def target(ctx, s):
	match = re.match(r"^[\?\!]\s?\w+\s+(.+)", ctx.content)
	if match != None:
		r = match.group(1)
		return re.sub('%T', r, s)
	return s

substrings = {
	'U': {'f': display_name,	'help': 'Message sender'},
	'T': {'f': target,			'help': 'Text after invocation'}
}

class SubstringInfo():
	def __init__(self):
		self.substrings = substrings

class Substring():
	def __init__(self, ctx, content):
		self.context = MessageData(ctx)
		self.input = content
		self.output = self._format()

	def _format(self):
		s = self.input
		for f in substrings.values():
			s = f['f'](self.context, s)
		return s

	def __str__(self):
		return self.output