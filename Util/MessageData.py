# General note: If a list only has one item it's only because we're leaving room for further expansion if needed (should not impact anything)

def display_name(ctx):
	for f in ['display_name', 'name']:
		if hasattr(ctx, f):
			return getattr(ctx, f)
	for f in ['author']:
		if hasattr(ctx, f):
			ctxf = getattr(ctx, f)
			return display_name(ctxf)

def content(ctx):
	if hasattr(ctx, 'message'):
		ctxf = getattr(ctx, 'message')
		return content(ctxf)
	return ctx.content

class MessageData():
	def __init__(self, ctx):
		self.display_name = display_name(ctx)
		self.content = content(ctx)