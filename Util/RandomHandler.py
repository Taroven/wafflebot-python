import random
import re
import numexpr
import time

eightball_phrases = [
	"It is certain", "Yes, but do it drunk",
	"Reply hazy, try again", "Don't swipe right, it's your cousin",
	"Don’t count on it", "I'd love to answer but I'm busy",
	"It is decidedly so", "I will tell you everything when you grow up",
	"Ask again later", "I don't know, what do you think?",
	"My reply is no", "Sometimes, the less you know the better",
	"Without a doubt", "You really don't want to know",
	"Better not tell you now", "Maybe, probably, if you want",
	"My sources say no", "Don't you have anything better to ask?",
	"Yes definitely", "What you need to know, you already know",
	"Cannot predict now", "Great, now my day is ruined",
	"Outlook not so good", "My lawyer says I shouldn't answer that",
	"You may rely on it", "All I know is my gut says maybe",
	"Concentrate and ask again", "Indubitably",
	"Very doubtful", "I'm trying really hard to avoid ambiguous questions at the moment",
	"As I see it, yes", "I'll leave that up to your imagination",
	"Most likely", "This space for sale, please contact -CORRUPTED-",
	"Outlook good", "Because the higher you go, the fewer",
	"Yes", "TMI",
	"Signs point to yes", "Sounds about right"
]

def roll(diceNum, diceAmount):
	r = 0
	for i in range(diceNum):
		r += random.randint(1,diceAmount)
	return r

def rollDice(match):
	a,b = match.group(1).split('d')
	x = roll(int(a), int(b)) # we can do this with a single random.randint() call individual rolls are theoretically more accurate to actual dice
	return str(x)

class RandomHandler():
	def __init__(self, data=1):
		random.seed()
		if isinstance(data, str | int):
			self.data = data
		else:
			self.data = list(data)

	def Get(self, **kwargs):
		iterations = kwargs.pop('iterations', 1)
		pop = kwargs.pop('pop', False)
		return self._pickrandom(iterations, pop)

	def DiceString(self):
		return re.sub(r"[^\d+\-*\/d]", '', self.data)

	def Dice(self):
		stripped = self.DiceString()
		rolled = re.sub(r'(\d+d\d+)', rollDice, stripped)
		r = numexpr.evaluate(rolled)
		return r

	def EightBall(self):
		return random.choice(eightball_phrases)

	def _pickrandom(self, iterations: int, pop: bool):
		temp = list(self.data)
		r = []
		if pop == True:
			iterations = min(iterations, len(temp))
		for x in range(iterations):
			y = random.randrange(len(temp))
			if pop == True:
				r.append(temp.pop(y))
			else:
				r.append(temp[y])
		return r