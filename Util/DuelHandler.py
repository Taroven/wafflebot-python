import random
random.seed()

# Key:
# - {a}: attacker
# - {d}: defender
# - {w}: winner
# - {l}: loser
attacks = [
	"{a} lunges forward with a fierce strike!",
	"{a} swings wildly, aiming for the head!",
	"{a} unleashes a rapid barrage of attacks!",
	"{a} charges forward, weapon raised high!",
	"{a} strikes with precision and speed!",
	"{a} aims a powerful blow!",
	"{a} feints left and strikes right!",
	"{a} delivers a swift and unexpected attack!",
	"{a} goes for a daring overhead swing!",
	"{a} strikes with all their might!",
	"{a} moves in for a quick jab!",
	"{a} executes a calculated attack!",
	"{a} slashes with lightning speed!",
	"{a} delivers a series of rapid strikes!",
	"{a} sweeps in with a low attack!",
	"{a} performs a spinning strike!",
	"{a} launches a surprise attack!",
	"{a} aims a precise thrust!",
	"{a} delivers a devastating blow!"
]
defends = [
	"{d} blocks the attack with skillful defense!",
	"{d} parries the strike with ease!",
	"{d} sidesteps, narrowly avoiding the blow!",
	"{d} counters with a swift defensive move!",
	"{d} ducks under the incoming attack!",
	"{d} braces themselves and stands firm!",
	"{d} deflects the strike with precision!",
	"{d} maneuvers gracefully out of harm's way!",
	"{d} anticipates the attack and dodges!",
	"{d} raises their shield just in time!",
	"{d} steps back, evading the strike!",
	"{d} reacts quickly, blocking the attack!",
	"{d} twists away from the oncoming blow!",
	"{d} parries and counters with finesse!",
	"{d} deflects the strike with a well-timed move!",
	"{d} swiftly moves to avoid the attack!",
	"{d} employs a defensive stance!",
	"{d} anticipates the strike and defends!",
	"{d} narrowly escapes the incoming strike!"
]
endings = [
	"{w} tickles {l} into surrender!",
	"{w} challenges {l} to a staring contest and wins!",
	"{w} and {l} engage in a tickle fight, with {w} emerging victorious!",
	"{w} wins by distracting {l} with a dance!",
	"{w} and {l} settle the duel with a thumb war, and {w} emerges triumphant!",
	"{w} and {l} engage in a game of rock-paper-scissors, with {w} winning!",
	"{w} wins by making {l} laugh uncontrollably!",
	"{w} and {l} engage in a game of charades, with {w} guessing right and winning!",
	"{w} and {l} decide to settle their differences with a game of tag, and {w} outruns {l} to claim victory!",
	"{w} wins by challenging {l} to a game of Simon Says and outsmarting them!",
	"{w} and {l} engage in a rap battle, with {w} dropping the sickest beats and emerging victorious!",
	"{w} and {l} engage in a marshmallow-eating contest, with {w} gobbling them up faster to win!",
	"{w} and {l} engage in a game of hide-and-seek, with {w} finding {l} first to win!",
	"{w} and {l} engage in a game of musical chairs, with {w} snagging the last seat to win!",
	"{w} and {l} decide to settle their differences with a game of tug-of-war, and {w} pulls harder to win!",
	"{w} wins by challenging {l} to a game of staring contest and hypnotizing them into submission!",
	"{w} and {l} engage in a game of hopscotch, with {w} hopping to victory!",
	"{w} and {l} engage in a game of 20 Questions, with {w} guessing the answer correctly to win!",
	"{w} and {l} engage in a game of Pictionary, with {w} drawing better to win!",
	"{w} wins by challenging {l} to a game of tag and outrunning them to the finish line to win!",
	"{w} and {l} engage in a game of truth or dare, with {w} daring {l} to admit defeat!",
	"{w} wins by challenging {l} to a game of Red Rover and breaking through their line to victory!",
	"{w} and {l} engage in a game of leapfrog, with {w} leaping over {l} to win!",
	"{w} wins by challenging {l} to a game of Marco Polo and outmaneuvering them to victory!",
	"{w} and {l} engage in a game of follow the leader, with {w} leading to victory!",
	"{w} wins by challenging {l} to a game of duck, duck, goose and tagging them to win!",
	"{w} and {l} engage in a game of Simon Says, with {w} outsmarting {l} to victory!",
	"{w} wins by challenging {l} to a game of Red Light, Green Light and racing to victory!",
	"{w} and {l} engage in a game of dodgeball, with {w} dodging to victory!",
	"{w} wins by challenging {l} to a game of tag and tagging {l} first to win!",
	"{w} and {l} engage in a game of dance-off, with {w} busting the best moves to win!",
	"{w} and {l} engage in a game of laugh-off, with {w} delivering the funniest jokes to win!",
	"{w} and {l} play charades, with {w} guessing the most correctly to win!",
	"{w} and {l} engage in a marshmallow-eating contest, with {w} devouring more to win!",
	"{w} and {l} play hide-and-seek, with {w} finding {l} first to win!",
	"{w} and {l} play tag, with {w} outrunning {l} to win!",
	"{w} and {l} play musical chairs, with {w} grabbing the last chair to win!",
	"{w} and {l} engage in a tug-of-war, with {w} pulling harder to win!",
	"{w} and {l} play hopscotch, with {w} hopping the longest to win!",
	"{w} and {l} engage in a game of 20 Questions, with {w} guessing correctly to win!",
	"{w} and {l} play Pictionary, with {w} drawing better to win!",
	"{w} and {l} have a truth or dare challenge, with {w} daring {l} to admit defeat!",
	"{w} and {l} play Red Rover, with {w} breaking through to victory!",
	"{w} and {l} play leapfrog, with {w} leaping over {l} to win!",
	"{w} and {l} play Marco Polo, with {w} outmaneuvering {l} to victory!",
	"{w} and {l} have a game of follow the leader, with {w} leading to victory!",
	"{w} and {l} play duck, duck, goose, with {w} dashing to victory!",
	"{w} and {l} play Simon Says, with {w} outsmarting {l} to win!",
	"{w} and {l} play Red Light, Green Light, with {w} sprinting to victory!",
	"{w} and {l} engage in a dodgeball match, with {w} dodging to victory!",
	"{w} and {l} play tag, with {w} tagging {l} first to win!"
]
settings = [
	"As {w} and {l} meet inside the towering clock tower...",
	"Upon the castle turret, {w} and {l} prepare for their duel...",
	"Amongst the ancient amphitheater ruins, {w} and {l} stand...",
	"Navigating the twists of the hedge maze, {w} and {l} converge...",
	"Exploring the depths of the sunken ship, {w} and {l} face off...",
	"Amidst the golden sunflowers, {w} and {l} prepare to duel...",
	"In the heart of the bustling bazaar, {w} and {l} prepare to fight...",
	"Near the bubbling volcanic crater, {w} and {l} prepare for battle...",
	"In the eerie midnight graveyard, {w} and {l} face each other...",
	"Within the hidden cave, {w} and {l} prepare to settle their dispute...",
	"Amongst the dusty tomes of the library, {w} and {l} prepare for their duel...",
	"On the precarious bridge, {w} and {l} stand ready to duel...",
	"Navigating the twisting labyrinth, {w} and {l} prepare to face off...",
	"In the opulent palace halls, {w} and {l} prepare for their duel...",
	"Beside the roaring waterfall, {w} and {l} prepare to fight...",
	"In the eerie moonlit marsh, {w} and {l} face each other...",
	"Upon the desolate theater stage, {w} and {l} prepare to duel...",
	"Within the dark underground city, {w} and {l} face off...",
	"Amongst the towering redwoods, {w} and {l} prepare to fight...",
	"Within the echoing cathedral chambers, {w} and {l} face each other...",
	"Atop the soaring cathedral spire, {w} and {l} prepare to duel...",
	"Amongst the remnants of the battlefield, {w} and {l} prepare to fight...",
	"In the shadow of the towering statue, {w} and {l} face each other...",
	"At the bottom of the dark well, {w} and {l} prepare to duel...",
	"Amidst the blossoming cherry trees, {w} and {l} prepare to fight...",
	"In the midst of the swirling sandstorm, {w} and {l} face each other...",
	"Amongst the crumbling city ruins, {w} and {l} prepare to duel...",
	"Amongst the floating sky islands, {w} and {l} prepare to fight...",
	"In the mystical garden, {w} and {l} prepare to duel...",
	"Within the magical arena, {w} and {l} prepare to face off..."
]

class DuelHandler():
	def __init__(self, user_a: str, user_b: str, strip_formatting: bool = False):
		self.strip_formatting = strip_formatting
		self.users = [user_a, user_b]

	def Duel(self):
		winner = random.randint(0,1)
		self.winner = self.users[winner]
		self.loser = self.users[winner ^ 1] # bit flips are cool
		return {
			'winner': self.winner,
			'battle': self.BattleString(self.winner, self.loser)
		}

	def BattleString(self, winner, loser):
		attacking = random.randint(0,1)
		attacker = self.users[attacking]
		defender = self.users[attacking ^ 1]
		# set the stage
		setting = random.choice(settings)
		attack = [
			random.choice(attacks),
			random.choice(defends),
			random.choice(attacks),
		]
		ending = random.choice(endings)
		# format it
		output = None
		if self.strip_formatting:
			output = setting + " " + " ".join(attack) + " " + ending
		else:
			output = setting + "\n- " + "\n- ".join(attack) + '\n\n' + ending
		return output.format(a = attacker, d = defender, w = self.winner, l = self.loser)
