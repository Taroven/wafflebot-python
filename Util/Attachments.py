import logging
from pathlib import Path
from urllib.parse import urljoin

log = logging.getLogger('WaffleBot-Util.Attachments')

class AttachmentHandler():
	def __init__(self, bot):
		self.bot = bot

	def GetAttachmentPath(self, file: str):
		return self.bot.AttachmentPath / file

	# TODO: Links are long. Shorten them.
	def GetAttachmentAddress(self, file: str):
		if self.bot.wwwAddress != None:
			short = '/' + self.bot.wwwAttachmentPath + '/'
			return urljoin(self.bot.wwwAddress, short + file)

	def RegisterAttachment(self, module: str, file: str):
		path = self.GetAttachmentPath(file)
		if path.exists():
			log.debug(f'Registering attachment {file} to {module}')
			data = self.bot.ReadGlobalData('Attachments')
			if data is None:
				data = {}
			if file not in data:
				data[file] = {}
			data[file][module] = True
			self.bot.WriteGlobalData('Attachments', data)

	def UnregisterAttachment(self, module: str, file: str):
		data = self.bot.ReadGlobalData('Attachments')
		if data is None:
			return
		if file in data:
			log.debug(f'Unregistering attachment {file} from {module}')
			data[file].pop(module, None)
			self.bot.WriteGlobalData('Attachments', data)
			if len(data[file]) == 0:
				self.DeleteAttachment(file)

	def DeleteAttachment(self, file: str):
		path = self.GetAttachmentPath(file)
		if path.exists():
			log.debug(f'Deleting attachment at {str(path)}')
			data = self.bot.ReadGlobalData('Attachments')
			data.pop(file, None)
			self.bot.WriteGlobalData('Attachments', data)
			path.unlink()
			return True

	def GetAttachmentPath(self, file: str):
		return self.bot.AttachmentPath / file

	def RemoveUnusedAttachments(self, **kwargs):
		key = kwargs.pop('key', 'attachment')
		data = self.bot.ReadGlobalData('Attachments')
		if data is None:
			log.debug('No attachments to clean up.')
			return
		attach_path = self.bot.AttachmentPath
		attach_list = list(attach_path.glob('*.*'))
		
		attachments = {}
		for attachment in attach_list:
			attachments[attachment.name] = 0
		for registered in data:
			attachments[registered] += 1
		for attachment, num in attachments.items():
			if num == 0:
				log.debug(f'Attachment {attachment} is unused, deleting.')
				self.DeleteAttachment(attachment)