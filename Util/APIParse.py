import requests
import re
import asyncio
import json

def APIGet(args):
	if 'params' in args and len(args['params']) > 0:
		try:
			args['response'] = requests.get(args['url'], params = args['params'], timeout = 10)
		except Exception as e:
			args['error'] = str(e)
	else:
		try:
			args['response'] = requests.get(args['url'], timeout = 5)
		except Exception as e:
			args['error'] = str(e)

def APIPost(args):
	try:
		args['response'] = requests.post(args['url'], **args['data'])
	except Exception as e:
		args['error'] = str(e)

# Build a formatted dict for a request field, for later retrieval in BuildFromAPI
def BuildRequestTree(content):
	r = {}
	current = r
	entries = content.split('.')
	for i, entry in enumerate(entries):
		match = re.match(r'^(\w+)\[', entry)
		if match is None:
			if i + 1 == len(entries):
				current[entry] = True
				return r
			else:
				current[entry] = {}
				current = current[entry]
		else:
			key = re.match(r'^(\w+)', entry)
			key = key.group(1)
			current[key] = {}
			current = current[key]
			matches = re.findall(r'\[(\w+)\]', entry)			
			for x, y in enumerate(matches):
				val = y
				if y.isdigit():
					val = int(y)
				if x + 1 == len(matches) and i + 1 == len(entries):
					current[val] = True
					return r
				else:
					current[val] = {}
					current = current[val]

def GetValueFromRequest(source, target):
	if type(source) is list: # handle list and dict separately since i'm lazy
		for key, val in target.items():
			if type(key) is int:
				if 0 <= key < len(source): # item exists in list
					if type(val) is bool: # jackpot
						return source[key]
					else: # dig deeper
						return GetValueFromRequest(source[key], target[key])
				else: # invalid
					return
	if type(source) is dict:
		for key, val in target.items():
			if key in source:
				if type(val) == bool: # jackpot
					return source[key]
				else: # dig deeper
					return GetValueFromRequest(source[key], target[key])
			else: # invalid
				return

class APIRequest():
	async def APIGet(self, url: str, **kwargs):
		request = { 'url': url }
		if 'params' in kwargs and kwargs['params'] != None:
			request['params'] = kwargs['params']
		await asyncio.gather( asyncio.to_thread(APIGet, request) )
		if 'response' in request:
			return request['response']
		elif 'error' in request:
			return request['error']

	async def APIPost(self, url: str, data):
		request = {
			'url': url,
			'data': data
		}
		await asyncio.gather( asyncio.to_thread(APIGet, request) )
		if 'response' in request:
			return request['response']
		elif 'error' in request:
			return request['error']

	async def GetAllFromAPI(self, url, params):
		response = await self.APIGet(url, params=params)
		if isinstance(response, str):
			return str
		if response.status_code == 200:
			try:
				response_dict = json.loads(response.text)
				return response_dict
			except Exception as e:
				print(str(e))
				return 'Something went wrong. The API returned non-JSON data.'
		else:
			return f'Something went wrong. The API returned code {response.status_code}.'

	async def BuildFromAPI(self, definition: dict):
		params = definition.pop('params', None)
		response = await self.APIGet(definition['api'], params=params)
		if isinstance(response, str):
			return str
		if response.status_code == 200:
			try:
				response_dict = json.loads(response.text)
			except Exception as e:
				print(str(e))
				return 'Something went wrong. The API returned non-JSON data.'
			content = definition['content']
			for field in definition['fields']:
				tree = BuildRequestTree(field)
				if tree != None:
					response_content = GetValueFromRequest(response_dict, tree)
					if response_content is None:
						response_content = '#ERROR#'
					content = re.sub(re.escape('$' + field), response_content, content)
			return content
		else:
			return f'Something went wrong. The API returned code {response.status_code}.'

	async def GetIGDB(self, game_id):
		if isinstance(game_id, int):
			data = {
				'headers': {
					'Client-ID': self.bot.Config.client_id,
					'Authorization': f'Bearer {self.bot.Config.token}'
				},
				'data': f'fields *; where id = {game_id}',
				'timeout': 10
			}
			response = await self.APIPost('https://api.igdb.com/v4/games', data)
			if isinstance(response, str):
				return str
			if response.status_code == 200:
				try:
					return json.loads(response.text)
				except Exception as e:
					print(str(e))
					return 'Something went wrong. The API returned non-JSON data.'