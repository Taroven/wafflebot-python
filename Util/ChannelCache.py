import logging
log = logging.getLogger('WaffleBot-Util.ChannelCache')

class ChannelCache():
	def __init__(self):
		self.UserLevelNames = [
			'Owner',
			'Admin',
			'Op',
			'Trusted',
			'Voice',
			'User'
		]
		self.channels = {}
		self.UserLevels = {}
		for i in range(len(self.UserLevelNames)):
			self.UserLevels[self.UserLevelNames[i]] = i

	def AddChannel(self, channel, owner):
		channel_string = str(channel)
		owner_string = str(owner)
		if channel_string not in self.channels:
			log.debug(f'ChannelCache: creating permissions for {channel}')
			self.channels[channel_string] = {
				'users': {},
				'roles': {}
			}
		self.SetUserLevel(channel_string, owner_string, 0)
		return self.channels[channel_string]

	def GetUserLevel(self, channel, user) -> int:
		channel_string = str(channel).lower()
		user_string = str(user).lower()
		if channel_string in self.channels:
			if user_string in self.channels[channel_string]['users']:
				return self.channels[channel_string]['users'][user_string]
			else:
				return self.UserLevels['User']
		else:
			return self.UserLevels['User']

	def SetUserLevel(self, channel, user, level: int):
		channel_string = str(channel).lower()
		user_string = str(user).lower()
		if channel_string in self.channels:
			self.channels[channel_string]['users'][user_string] = level

	def GetRoleLevel(self, channel, role) -> int:
		channel_string = str(channel)
		role_string = str(role)
		if channel_string in self.channels:
			if role_string in self.channels[channel_string]['roles']:
				return self.channels[channel_string]['roles'][role_string]
			else:
				return self.UserLevels['User']
		else:
			return self.UserLevels['User']

	def SetRoleLevel(self, channel, role, level: int):
		channel_string = str(channel)
		role_string = str(role)
		if channel_string in self.channels:
			self.channels[channel_string]['roles'][role_string] = level
