import random
import time

Games = {}
game = lambda f: Games.setdefault(f.__name__, f)

roulette_payouts = [1,2,4,8,16]
roulette_weights = [60,20,8,4,2]
@game
def roulette(**kwargs):
	odds = kwargs.pop('roulette_odds', 3)
	data = {
		'game': 'roulette',
		'bid': kwargs['bid'],
		'mult': random.choices(roulette_payouts, weights=roulette_weights)[0],
		'win': True if random.randint(0,odds) < 1 else False
	}
	return data

slots_emoji = ["🙂", "😂", "🤣", "😕", "🤩", "🌠"]
slots_symbols = 5
slots_payout = 15
slots_wild_chance = 5
slots_wild_payout = 250
def roll_slot(symbols):
	return 'w' if random.randint(1,100) <= slots_wild_chance else random.randint(1, symbols)

@game
def slots(**kwargs):
	symbols = kwargs.pop('slots_symbols', slots_symbols)
	result = []
	for i in range(3):
		result.append(roll_slot(symbols))
	matches = 0
	wilds = 0
	payout = slots_wild_payout
	for i in range(1,slots_symbols + 1):
		c = result.count(i)
		if c > matches:
			matches = c
	slots_output = []
	for v in result:
		if type(v) == int:
			payout = v * slots_payout
			slots_output.append(slots_emoji[v - 1])
		else:
			wilds += 1
			slots_output.append(slots_emoji[-1])
	data = {
		'game': 'slots',
		'bid': kwargs['bid'],
		'slots': slots_output,
		'mult': payout,
		'win': False
	}
	if wilds + matches == 3:
		data['win'] = True
	return data

class Gamble():
	def __init__(self, **kwargs):
		random.seed()
		self.options = kwargs
		if 'game' in self.options:
			self.options['game'] = self.options['game'].strip().lower()

	def GetGames(self):
		return [*Games]

	def Play(self):
		if 'game' in self.options and self.options['game'] in Games:
			return Games[self.options['game']](**self.options)